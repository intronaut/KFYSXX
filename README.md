```
 ██   ██ ████████ ██    ██  ████████
░██  ██ ░██░░░░░ ░░██  ██  ██░░░░░░
░██ ██  ░██       ░░████  ░██
░████   ░███████   ░░██   ░█████████
░██░██  ░██░░░░     ░██   ░░░░░░░░██
░██░░██ ░██         ░██          ░██
░██ ░░██░██         ░██    ████████
░░   ░░ ░░          ░░    ░░░░░░░░
```
```text
symbols: →↑←↓↔
greek: αβγδΔεκλμσπτρφχ
logic: ∴ ∵
math: − ∓ ∙ √ ∛ ∜ ∞ ∟ ⊾ ∠ ∡ ⊾ ≈ ≙ ≡ ≤ ≥ ∅
```

```
Hertentamen voeding & gezondheid B (vrijdag 12 juni 08:30)  May 29, 2020 11:51
Beste studenten,
 

Op vrijdag 12 juni om 08:30 staat het hertentamen van Voeding en Gezondheid B ingepland. Het tentamen bestaat uit 20 meerkeuze vragen en 5 korte open vragen. Het tentamen wordt via Brightspace afgenomen. Om 08:30 gaat het hertentamen open en kun je de toets starten en de vragen beantwoorden. Je hebt 1 uur en 20 minuten (80 minuten) om het tentamen te maken, daarna sluit de toets. Zorg dus dat je tijdig alles gereed hebt en toegang hebt tot een stabiele internetverbinding!
 

De toets vind je terug in de Brightspace course 1920 Voeding en Gezondheid B (KK2 V):

https://brightspace.ru.nl/d2l/home/93595

Op vrijdag 12 juni om 08:30 vind je onder “Toets” (bij Table of Contents) de toets beschikbaar en kun je deze starten.

De vragen en antwoordopties zullen in het Engels zijn.
Je hebt voor het tentamen 1 uur en 20 minuten.
Let op: wanneer je een antwoord gegeven hebt, kun je NIET meer terug naar deze vraag. Dus lees de vraag én antwoord opties goed, voordat je antwoordt en je verder gaat.

Tijdens dit tentamen vindt géén proctoring plaats, het is echter niet toegestaan tijdens dit tentamen ingelogd te zijn in de MOOC. We vertrouwen hierbij op jullie wetenschappelijke integriteit. Mochten de logboek gegevens van de MOOC of Brightspace aanleiding geven tot twijfel aan je integriteit, wordt dit gemeld bij de examencommissie.

De leerstof van het tentamen Voeding & Gezondheid B omvat de volgende modules van de MOOC Nutrition and Health: Macronutrients and Overnutrition

Module 5 Lipids and Health
Module 6 Proteins and Health
Module 7 Energy homeostasis and energy balance
Module 8 Weight management
 

Heb je vragen over de toets, neem dan contact op met Paul.Heijnen@radboudumc.nl .

Mocht je tijdens het tentamen op vrijdag 12 juni technische problemen ondervinden neem dan zo snel mogelijk contact op met Paul Heijnen. Dit is per mail (Paul.Heijnen@radboudumc.nl ) of telefonisch (06 82086285) mogelijk.

 

Met vriendelijke groet,

Maria Hopman
Renée van Damme
Paul Heijnen
```

## Voeding & Gezondheid B -- Elderly, proteins, exercise

### Glossary

|    abbr    |                                                                                 def                                                                                  |
|------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| sarcopenia | [Sarcopenia](https://en.wikipedia.org/wiki/Sarcopenia) is the degenerative loss of skeletal muscle mass, quality, and strength associated with aging and immobility. |
| CHD        | coronary heart disease = ischaemic heart disease (IHD)                                                                                                               |
| RDA        | recommended dietary allowance                                                                                                                                        |
| MPS        | muscle protein synthesis                                                                                                                                             |
| PE         | physical exercise                                                                                                                                                    |
| SFA        | saturated fatty acids                                                                                                                                                |
| UFA        | unsaturated fatty acids                                                                                                                                              |
| FFA        | free fatty acids                                                                                                                                                     |
| TAG        | triglyceride                                                                                                                                                         |
| MUFA       | monosaturated fatty acid                                                                                                                                             |
| PUFA       | polysaturated fatty acid                                                                                                                                             |
| Ω-3FA      | Omega-3 fatty acid                                                                                                                                                   |

### Scrap knowledge

`=> aantekeningen uit HC`

#### Recommended daily amount of protein

|  age (y)  | g/kg/d[^1] |
|-----------|------------|
| <65       |        0.8 |
| >65       |        1.0 |
| >65 __*__ |        1.2 |

__*)__ 65 years old _and active_

### Summary

- "_door lage proteine intake zijn er minder positieve effecten van bewegen op spiermassa_"
- Sarcopenia is a common clincial problem that is becoming of greater concert as the older population continues to grow
- Mounting evidence suggests that the RDA (Recommended Dietary Allowance) for protein, designed to prevent deficiency, is inadequate to promote optimal health in older adults
- New research has focused on the quantity, quality, and timing of protein intake to best stimulate muscle protein synthessis (MPS) in older adults.
- There remains a need for large, long-term, RCTs examining whether the positive effects of dietary protein on muscle metabolism in acute studies will translate over the long term into gains of muscle mass, function, and the overall health of older adults.

**Walking exercise = 4-daagse**

- only significant result:
    + differences in lean body mass over time !

_ _ _ _

### 5. Lipids and Health ###

#### 5.1 intro ####

- Cardiovascular disease (CVD)
- CVD risk factor
- Modulating blood lipids via diet
    + [chol]
        * LDL
        * HDL
    + sat. fat
    + unsat. fat
    + transfat. acid ("_killer fats_")
        * Dr. Martijn Katan
    + omega-3 FA
- General recommendations



- Health effect of lipids
    + dietary lipids and CVD
        * what is it
        * how does it develop
        * major risk factors for CVD
        * atheroscleoriss
- Modulating blood lipids via diet
    + [chol]
        * LDL (high LDL = CVD risk up) 
            - [LDL] in diet vs. in blood
            - sat/unsat fats in diet vs. in blood
        * HDL
        *   
    + transfat. acid ("_killer fats_")
        * Dr. Martijn Katan - cholersterol-raising effect of trans. fat
        * sat. fat vs unsat. fat
            - healthy or unhealthy
    + omega-3 FA
        * latest insights
    + Specific recommendation for dietary fat intake

#### Learning outcomes ####

- recognize that the incidence of CVD varies hugely between countries, pointing to an environmental cause.
- describe the key features of atherosclerosis
- describe causes and remedies for hypertension
- understand and appreciate the importance of [LDL]s as a risk factor for heart disease
- understand the impact of dietary lipids on plasma lipids \(([lipid]p))
- understand the basis for the current controversy on dietary lipids and heart disease

#### 5.2 Cardiovascular disease and atherosclerosis ####

#### Cardiovascular disease (CVD) and Coronary heart disease (CHD) ####

- CVD (cardiovascular disease, heart disease)
    + most common form: coronary heart disease (CHD)
        * blood vessesls providing to the heart muscle become narrow and rigid, restricting blood flow to the heart
        * most often rooted in atherosclerosis
            - build-up of plaque in the walls of arteries, leading to narrowing fo the arteries and gradual obstruction of blood flow
        * other CVDs:
            - heart failure
            - diseases of the heart muscle (cardiomyopathy)
            - heart vaqlve problems
            - arrhythmias

- CHD
    + **leading cause of death in high-income countries**
    + vs. **infections are major cause of death in low-income countries**
    + responsible factors; differences in:
        * abundance of infectious organisms
        * hygiene
        * medical care
        * diet

![CHD-world-prvl](./src/CHD-world-prvl.png]

- Above picture shows age-standardised, DALY rates from CHD by country (per 100 000 inhabitants; the darker the color, the higher the rates).
- 1 DALY represesnts one lost year of "healthy" life.
    + WHO definition: _The sum of DALYs across the population, or the burden of desease, can be thought of as a measurement of the gap between current health status and an ideal health situation where the entire population lives to an advanced age, free of disease and disability._
    + Highest DALY rates for ischaemic heart disease are observed in eastern Europe.

<!-- ![Coronary heart disease](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/710fc42cd7d72ff46ae8957e3d30bda7/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/CVD1.png) -->

![CVD3](./src/CVD3.png)

- Above pictures shows the age standardized CHD death rates per 100 000).
- Calculated differently than the DALY, but shows similar overall picture
    + highest rates in eastern Europe, lowest rates in France + Japan

- Trends in deaths from CVD show striking pattern
    + many western + northern European countries, which had the highest CVD-rates
    + => now: sharp decline in CVD deaths, _despite growing prevalence of obesity_
        * decrease in CVD deaths is accounted for by:
            - huge reduction in deaths from CHD and stroke:
                * => improvements in long-term and emergency medical care
                * => decreased smoking
                * => behavioural adjustments towards diets and exercise are **unlikely** to have importantly contributed towards decline
    + substantial increase in CVD-rates in eastern European countries
        * tobacco use
        * dietary behavrious
        * alcohol use
        * poor medical care
        * (difficult to assign main culprit)


<!-- ##### Ischaemic heart disease death rates ##### -->

<!--
- trends in CVD-deaths show striking pattern
    + Many western and norhtern European countries, which orginally had the highest rates of CVD, there has been a sharp decline in CVD deaths, despite the growing prevalence of obesity. It is largely explained by improvements in long-term and emergency medical care and decreased smoking in recent years. Behavioural adjustments towards diets and exercise are unlikely to have importantly contributed towards the decline.
-->

#### Atherosclerosis

<!-- ![Atherosclerosis](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/b80f1c046ca1bad9ec457e148a2f5f13/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/Blausen_0257_CoronaryArtery_Plaque.png) -->

![CVD-atherosclerosis](./src/CVD-atherosclerosis.png)

- normal artery contains three layers:
    + inner: endothelium/tunica intima
    + middle: smooth muscle cells/tunica media
    + outer: connective tissue/tunica externa
- plaques begin to form because of damage to endothelium, because of:
    + elevated lipid levels in blood
    + high blood pressure
    + smoking
- damage causes white blood cells to stick to endothelium
    + (endothelium produces sticky molecules called adhesion molecules that capture WBCs))
    + after adhesion, WBCs (including T-cells and macrophages) move inside arterial wall
    + vessel wall gradually thickens as it fills up with lipid
    + SMCs move to the intima ands produce molecules such as _collagen_
    + in advancing laesions cells die and cell debris (including lipids) accumulates in the central region of the plaque => lipid core / necrotic core

![CVD-plaquebuildup](./src/CVD-plaquebuildup.png)


- lipids that accumulate in the atherosclerotic plaque primarily originate from LDL, which is able to penetrate the wall of the artery and become scavenged by macrophages.
- In turn, macrophages become foam cells and start to produce molecules that aggravate the inflammation

![CVD-foamcells](./src/CVD-foamcells.png)

- Above picture illustrates tow key features in atherosclerosis
    + \1. adhesion and infiltration of the immune cells into the vascular wall
    + \2. entry of LDL particles into vascular wall and formation of foam cells
- "lipid hypothesis" high blood cholesterol levens contribute causally to atherosclerosis and CHD
- "inflammatory hypothesis" immunce system and inflammation play an important role in the development of atherosclerosis, based on experimental, clinical and epidemimological studies.

#### Thrombosis

- most atherosclerotic lesions are stable and do not cause any problems
    + the lipid core is covered by thick layer of materials found in connective tissue (fibrous cap)
- however, when lesion is very inflamed, WBCs gradually break down the fibrous cap
    + thinning of fibrous cap causes unstable plaques, making it prone to rupture
    + rupture is dangerous: causes thrombosis and/or (complete) occlusion or embolism

![CVD-thrombosis](./src/CVD-thrombosis.png)

- Process of blood coagulation is controlled by certain **eicosanoids**
    + eicosanoids are made from poly-unsaturataed fatty acids (poly-UFAs)
        * because of this, types of poly-UFAs in diet influence the types of eicosanoids produced. => dietary FA composition may influence blood clotting.
    + NSAIDs inhibit eicosanoid systhesis (blocking COX-enzyme), leading to impaired blood clotting
        * for this reason, NSDAIDs are extensively used in primary prevention and secondary prevention

<!-- ![CVD-atherosclerosis](./src/CVD-atherosclerosis.png) -->

#### 5.3 CVD risk factors

- most of risk factors for CHD are also risk factors for the other diseases that fall under CVD.
    + **Age**: is a major risk factor for CHD (atherosclerosis is a progressive disease and takes years to develop)
    + **Family history**: CHD often runs in families
        * part of the effect of FamHx occurs via traditional risk factors (such as blood [chol], which is in part genetically determined).
        * recent GWAS indicate that **majority of CHD-contributing genes function independently of conventional risk factors**
    + **Sex**: males have a higher risk of of developing CHD.
        * However, gender difference is gradually becoming erased
        * CHD is the #1 killer for both men and women in many countries
    + Many risk factors for CHD are modifiable
        * majory of these risk factors is highly interrelated, ..
        * .. but also seem to have an independent effect on CHD
            - e.g. Physical activity may lower CHD by decreasing obesity/diabetes and improving blood lipid levels, but may also exert an effect independent of these factors
    + Risk of CHD can be greatly reduced by behavioural changes, cornerstones:
        * lowering blood pressure
        * reducing blood cholesterol (LDL)
        * weight loss
        * increased physical exercise
        * abstinence from smoking
        * avoiding stress
        * consuming a healthy diet

#### Hypertension

- Hypertension is an important risk factor for CVD.
- Many people develop hypertension as they age (as their arteries become more rigid)
- Many people have hypertension without knowing it (often referred to as the sijlent killer)
- Chronic hypertensinon can exchaust the heart muscle and lead to heart failure
    + BP,sys,N = 110 - 130 mmHg
    + BP,dia,N = 70 - 80 mmHg
- A number of factors are well known:
    + genetics
    + age
    + lack of exercise
    + poor diet
    + obesity
    + excess alcohol consumption
- A number of relationships are less clear (classified as 'possible')
    + stress
    + smoking
    + sleep apnea

- Treatment is primarily in lifestyle changes
- If changes in lifestyle fail or are not sufficiently effective pharmaceutical treatment is next in line

<!--
 #### 5.3 CVD risk factors ####

"_Another major risk factor for CHD is family history: CHD often runs in families. Part of the effect of family history occurs via traditional risk factors (such as blood cholesterol, which is in part genetically determined). Recent so called Genome Wide Association Studies (GWAS) indicate that the majority of genes contributing to CHD function independently of conventional risk factors. How these genes impact CHD is unknown._" -- source: [edX](https://edge.edx.org/courses/course-v1:WageningenX+NUTR101+3T2019/courseware/76d1d42cae1a4650a8c4de1189191240/e069139122424077865c4044af9c9b2e/?child=first)

- males have a higher risk of developing CHD. However, the sex difference is gradually "becoming erased"
- Nonetheless, CHD is the #1 killer for both males and females in many countries.
    + In the interview: "Cancer is now the #1 killer in NL, where CHD was #1 ~50 years ago"

##### CVD -- Modifiable risk factors #####

- BP↓
- \[LDL]s↓
- m<sub>body</sub>↓
- PE↑
- abstinence from (cigarette) smoking
- avoiding stress
- healthy diet

→↑←↓↔


##### BP contributors #####

- genetics
- age↑
- PE↓
- poor diet
- m<sub>body</sub>↑
- excess \[EtOH] consumption
-->

#### Blood cholesterol as CHD risk factor

- Cholesterol is a lipid; so is carried by special molecules
- LDL (carries ~60--70% of total blood cholesterol)
    + it delivers cholesterol from liver to tissues
- HDL (carries ~30--40% of total blood cholesterol)
    + it delivers cholesterol from tissues to liver
        * (i.e. 'picks up' cholesterol)
- Total blood cholesterol levels correlate very well with [LDL]s

- **Traditionally**
    + LDL = 'bad' cholesterol
    + HDL = 'good' cholesterol
    + .. needes some re-evaluation


- **Different evidences linking LDL to CHD**  
    + Epidemiological
        * High LDL is correlated with higher CHD risk
    + Genetic 
        * People with genetically high LDL levels have higher risk for CHD
        * People with genetically _low_ LDL levels have lower risk for CHD
    + Pharmacological
        * Lowering of LDL using drugs reduces CHD risk (statins)
    + Mechanistic
        * LDL is taken up by macrophages in atherosclerotic plaques to form foam cells

- **Different evidences linking HDL to CHD**  
    + Epidemiological
        * High HDL is correlated with lower CHD risk
    + Genetic (**not very conclusive**)
        * People with genetically high HDL levels do **not** have a lower risk for CHD
    + Pharmacological
        * Raising HDL using drugs does **not** reduce CHD risk; but **actually increases risk for CHD !**
    + Mechanistic
        * HDL takes cholesterol from macrophages and returns it to the liver

- **Conclusions**
    + we can't definitely conclude that HDL is protective
    + we can definitely conclude that LDL cholesterol raises your chance of heart disease

#### 5.4 Dietary cholesterol

- Blood lipid profiles (esp. LDL) are fairly sensitive to changes in dietary fat composition.
- The amount of cholesterol, trans fatty acids, saturaed fatty acids and unsaturated fatty acids in the diet can alter the levels of lipoproteins in the blood.

#### Cholesterol from the diet

- **Recommendations regarding cholesterol in the diet have undergone some changes troughout the years**
    + it became evident that blood cholesterol levels are rather **insensitive** to changes in dietary cholesterol intake, partly because dietary cholesterol contributes only about 1/3 of the cholesterol content in the body.
    + Katan and Beynen demonstrated that the extent to which changes in cholesterol intake influence blood cholesterol levels differs markedly between individuals.
        * In most individuals, increase in cholesterol intake has little impact on blood cholesterol levels
        * However, in about 1/3 - 1/4 of the population, blood cholesterol levels do go up.
    + More recent studies: relation between dietary cholesterol and blood cholesterol is determined by genetics via differences in fractional cholesterol absorption.
        * Fractional cholesterol absorption represtents the proportion of cholesterol in the intestine that is absorbed in the body, which **varies from 30 to 80% among humans**.
        * => so: some people are very efficient at taking up cholesterol, whereas other people are relatively inefficient.

- General guideline: AHA recommends limiting cholesterol intake to <300mg/day for most people.
    + regardless, US dietary guidelines 2015-2020 have abandoned making any recommendations on cholesterol based on the current evidence indicating that dietary cholesterol (at current inttakes) does not increase the risk of heart desease in healthy individuals.
    + in NL: already decided against implementing any guidelines for dietary cholesterol in 2006.
- New guidelines convey that cholesterol is much less of a concern than it was protrayed to be in the past. **Some caution is warrented**, though. 
    + There is evidence that high intake of cholesterol may increase risk of CVD in people at high risk of developing the disease, such as DM2.

#### Lowering cholesterol uptake

- Lower **cholesterol uptake** by:
    + Avoiding cholesterol-rich foods (eggs, organ meats, shellfish)
    + Increased intake of fibres (pectins, psyllium, beta-glucans, possibly inulin)
    + Plant sterols and stanols (at doses present in functional foods) decrease cholesterol absorption.
        * Plant sterols and stanols' structure is similar to cholesterol and compete with cholesterol for uptake in the intestine



<!-- > It subsequently became evident that **blood cholesterol levels are rather insensitive to changes in dietary cholesterol intake**, partly because **dietary cholesterol contributes only about 1/3 of the cholesterol content in the body**. Katan and Beynen from the Division of Human Nutrition at Wageningen University demonstrated that the extent to which **changes in cholesterol intake influence blood cholesterol levels differs markedly between individuals**. In **most individuals, an increase in cholesterol intake has little impact on blood cholesterol levels**. However, in about **1/3 to 1/4 of the population, blood cholesterol levels do go up**. More recent studies suggest that the relation between **dietary cholesterol and blood cholesterol is determined by genetics via differences in fractional cholesterol absorption**. The fractional cholesterol absorption represents the proportion of cholesterol in the intestine that is absorbed into the body, which **varies from 30 to 80% among humans**. What it means is that some people are very efficient at taking up cholesterol into their bodies, whereas other people are relatively inefficient. In people that are very efficient at taking up cholesterol, an increase in cholesterol intake is more likely to translate into an increase in blood cholesterol.

source: [edX](https://edge.edx.org/courses/course-v1:WageningenX+NUTR101+3T2019/courseware/76d1d42cae1a4650a8c4de1189191240/0d54e5354511420e9d156383c0e27477/?child=first)

- Intake of fibres (pectins, psyllium, ß-glucans, (inulin) ) can reduce \[chol]s.
- plant sterols and stanols (at doses present in fucntinoal foods) decrease \[chol]-absorption. Plant sterols and stanols compete with cholesterol for uptake in the intestine.
- Regardless of eating cholesterol-containing foods or not, the largest amount of cholesterol present in the guts is cholesterol from bile. This means that, regardless of cholesterol consumption, lowering cholesterol uptake is still a relevant/significant strategy in reducing CHD risk (in the future).


![enterohepatic](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/d8489e4a2e62608fcdecb49765e3b235/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/Intestinal_cholesterol_metabolism.PNG)
-->

- **Most of cholesterol present in our GI tracts does not come from consumed foods, but enters through the enterohepatic cycle**
    + this means:
        * even without consuming cholesterol-containing foods, there is still a substantial amount of cholesterol in your intestines
        * even with **ZERO** cholesterol intake, you may still benefit from strategies to lower cholesterol **uptake**

![CVD-enterohepatic](./src/CVD-enterohepatic.png)

#### Saturated / unsaturated fatty acids (SFAs / UFAs)

- Mainstay of our dietary recommendations for decades:
    + **Reducing intake of saturated fat**
- Lately, this recommendation has been under attack:
    + Scientists have questioned wheter saturated fat prommotes CHD and have argued that **limitation of refined carbohydrates intakes** and **reduction in excess adiposity** are much more important
    + The notion that saturated fat is not harmful or even protective against CHD is broadly broadcasted over the internet and has numerous very vocal proponents.

- Scientific basis for the recommendation to **limit intake of saturated fat**
    + Ancel Keys - research into the link between SFA and CHD
        * He showed that relative occurence of heart disease is positively correlated with the percentage of calories in the diet that comes from SFA. (see picture below)
        * Also found that as Japanese men migrated from native Japan to Hawaii or California (markedly increasing their SFA intake), their **risk for CHD went up** dramatically, **together with a marked increase in blood cholesterol levels**. (this study, however, provides a weak indication)
    + Ancel Keys' epidemiological studies can be used to **generate hypotheses**, but not to actually **test hypotheses** (population-based associations canont be exprapolated to the individual level).
    + Accordingly, Ancel Keys' studies shouuld not be presented as evidence in the ongoing discussion on saturated fat and heart disease.

![CVD-SFA-CHD](./src/CVD-SFA-CHD.png)

- After cross-cultural comparisons and migration studies (such as Ancel Keys'), prospective studies have failed to find a positive relation between SFA intake and CHD.
- Even many of these studies don't clearly reveal that SFA intake is linked to CHD, leading to **two possible conclusions**:
    + \1. a high SFA intake is not associated with elevated risk of CHD
    + \2. the epidemiological research methods available are insufficient and would not have picked up a link between SFA and CHD in the population studied, even if there was one.

- Option 2 in detail:
    + In prospective studies:
        * individuals write down their dietary habits at the beginning of the study (allowing estimation of their SFA intake) and are subsequently followed for years/decades for the occurence of illness or death.
    + Prospective studies have intrinsic limitations, e.g.:
        * people with high SFA intake may have other dietary or non-dietary habits that differ from people with low SFA intake => confounding
        * recording of dietary intake may be problematic, people have difficulty remembering what they ate, or consciously or subconsciously write down something else. (This is a mamjor caveat in nutritional epidemiology and is not just limited to the link between dietary fat and heart disease.)
        * **Serious limitation of contemporary prospective studies**: the overall intake of SFA in general population is considerably lower than it was 30-40y ago AND difference in SFA intake inbetween individuals is rather small.
        * Additional key issue: what do you compare SFA with?
            - in practice: people will replace SFA with UFA or carbohydrates
            - analysis of connection between SFA and CHD only has meaning if you take these replacements into account.
    + Taking into account the above considerations:
        * Prospective cohort studies have found that **replacement of SFA with MUFA or PUFA is associated with a lower risk of CHD**.
        * By contrast, **no decrease in CHD risk is observed if SFA is replaced with refined carbs**.
            - Refined carbs cover the carbohydrateds present in: white flour, white bread, white rice, sodas, sweets, breakfast cereals, added sugars.

- Interventions on SFA and blood lipids
    + important consideration: what do you excange SFA with?
    + in dietary interventions, you cannot add SFA to the diet, without removing something else.
    + In practice, SFA are replaced by either carbohydrates or UFAs.
        * Numerous dietary interventions have shown that **removing SFA in favor of carbohydrate lowers erum levels of LDL and HDL**.
        * When **comparing with UFAs, SFA markedly raise LDL and to a smaller extent also HDL**.
            - SFA that most potently raise LDL levels: mystiric acid, lauric acid, palmitic acid.
    + **Given the causal role of LDL, a reduction in serum LDL levels is predicted to have a beneficial effect on CHD risk**.
    + Another small caveat:
        * this is **no direct effect**, SFA intake can (theoretically) increase/decrease, without taking blood LDL levels into account, even when blood LDL levels **DO change**.

- Bottom line
    + PUFA from vegetable oils replacing SFA from dairy and meat, **lowers CHD risk**
    + Replacing SFA intake with (refined) carbohydrate intake, **does not lower the risk of CHD**.
    + **SFA raises levels of LDL**, a causal risk factor for atherosclerosis and CHD. **Replacing SFA with MUFA or PUFA lowers LDL levels**.
    + **Replacing SFA with PUFA prevents and regresses atherosclerosis in non-human primates**.
- In agreement with this:   
    + current guidelines call for replacement of dietary saturated fatty acids by unsaturated fatty acids.

<!-- 
- Prospective research into SFA-consumption is tricky, because nowadays, people eat drastically lower amounts of SFA than 30--40 years ago.
    + replace SFA with MUFA or PUFA => CHD-risk lower (+)
    + replace SFA with refined carbs => CHD-risk equal (=)_

> Recently, an advisory committee of the American Heart Association went through the available scientific evidence and reached the following conclusions in their highly publicized report:
>
> - Randomized clinical trials showed that PUFA from vegetable oils replacing SFA from dairy and meat lower risk of CHD.
> - A dietary strategy that consists of reducing intake of SFA by replacement with (refined) carbohydrates does not lower risk of CHD.
> - SFA raises levels of LDL, a causal risk factor for atherosclerosis and CHD. Replacing SFA with MUFA or PUFA lowers LDL levels.
> - Replacing SFA with MUFA or PUFA lowers blood triglyceride levels, an independent biomarker of risk of CHD.
> - Replacing SFA with PUFA prevents and regresses atherosclerosis in non-human primates.
-->

#### Trans Fatty Acids

- Hydrogenation of fats is an industrial procedure that makes the fat less likely to spoil and betows favourable sensory and textural properties.
- Consumption of industrial trans fat has been linked with numerous untoward health consequences, including cancer, diabetes, CHD.
- Stimulation of CHD by industrial trans fat occurs at least partially via changes in plasma lipid levels
    + Studies at Wageningen University about 28y ago showed that industrial trans fat **markedly elevate blood LDL levels**.

- Unilever was quick to apply new findings and eliminated them from most foods.
    + Purely private initiative without any government intervention
- In contrast, US food industry was initially in denial ..
    + .. and initiated their own study to try to debunk the new findings on trans fat and blood cholesterol
    + study published in 1994 showed:
        * trans fatty acids increasesd blood LDL levels
        * '' decreased HDL levels
        * US had promoted margarines as heart-healthy alternative to butter, but was now shown to promote CHD.

- Fast food was traditionally loaded with trans fat.
- Also many processed foods (desserts, french fries, doughnuts, pastries, cookies, crackers, microwave popcorn products, frozen pizzas, margarines, coffie creamer) contained lots of trans fat.
    + highest trans fatty acid content was in frying oil (up to 50%)
- Since trans fat gained a bad reputation, many manufacturers and retailers have managed to substantially decrease trans fatty acid levels in many foods and products without compromising their taste and stability
- _"The Dutch Nutrition Survey from 2018 indicated that trans fatty acids provide approximately **0.3% of the daily energy requirement, which used to be around 5-10% several decades ago.**"_

- In 2015, FDA determined that partially hydrogenated oils (_are the primary dietary source of industrial trans fatty acids_) are not generally recognised as safe (GRAS) for any use in food, based on new scientific evidence and the findings of expert scientific panels.
    + As of Jan 1 2020, manufacturers cannot add partially hydrogenated oils to foods.
- EU never took legislative action.
    + instead, EU food industries voluntarily committed themselves to reduce trans fatty acid content of foods up to a maximum of 2% of total fat content.
        * No mandatory labeling of trans fat.
    + overall trans fatty acid content of food has dropped, lack of legislation has prevented many food manufacturers (mostly in Eastern Europe) from taking action.
    + residual presence of trans fat in EU should drive legislators to consider additional public health approaches to reduce trans fat intake in Europe: legal limits, mandatory labeling, voluntary food reformulation pledges.
        * New proposal in Oct 2018: maximum limit for trans fat content in foodstuffs of 2%; non-complying foods may be placed on the market until April 1 2021.

- Trans fatty acid intake from industrially hardened oils has fallen below the trans fatty acid intake from dairy fat and meat (in Western countries)
    + further attempts to lower trans fatty acid intake therefore also needs to target intake of whole-fat milk, yoghurt, cheese, butter, as well as animal fat as part of beef and mutton.
    + New EU proposal specifically leaves out trans fat naturally occurring in animal fat.

#### Omega-3 fatty acids (n-3 PUFA, Ω-3FA)

- n-3 PUFA (omega 3 poly-unsaturated fatty acids)
    + alhpa-linolenic acid (ALA)
        * found in plkants (flaxseed, walnut, canola, soybean oil)
    + eicosapentanoic acid (EPA)
    + docosahexanoic acid (DHA)
        * EPA + DHA are marine sources (anchovies, mackerel, salmon, sardines)

![n-3_PUFA](./src/n3PUFA.png)

- n-3 PUFA lowers blood triglyceride levels, probably by reducing VLDL productin in liver and stimulating VLDL metabolismn in muscle and fat tissue.
    + Effects of n-3 PUFA on blood LDL and HDL are very minor or non-existent.


- **History of n-3 PUFAs**
- recent meta-anlysis reaced the conclusion that n-3 PUFA supplementation is not associated with a lower risk of all-cause mortality, cardiac death, sudden death and myocardial infarction
    + provoked heavy criticism from other scientists on the methodological shortcomings and interpretation of the study.
- another meta-analysis concluded that prospective epidemiological studies and interventions provide strong evidence that n-3 PUFA decrease the risk of fatal CHD ..
    + .. whereas effects on measures of heart dysfunction (fibrillation, arrhythmia) are uncertain.
- **Current status on n-3 PUFAs**
    + it has become increasingly difficult to demonstrate a benefit of n-3 PUFA
        * overall incidence of CHD has decreased and post-MI treatment has improved
        * To accumulate sufficient numbers, a strategy has been: focussing studies on subjects that already suffered a heart attack (secondary prevention)
            - nowadays, treatment options are excellent and patients rarely suffer a second heart attack.
    + Including subjects that never had a heart attack (primary prevention) for studying the effect of n-3 PUFA would be ideal. The problem is that you would have to include a huge number of subjects to accumulate a sufficient number of heart attacks.
- Natural medicines Comprehensive Database (service from US National Library of Medicine) rates effectiveness based on scientific evidence according to a scale
    + n-3 PUFA were rated as:
        * effective for high triglycerides
        * likely effective for heart disease
        * possibly effictive for high blood pressure, rheumatoid arthritis, weight loss (among others)

- CHD risk reduction
    + **how n-3 PUFA reduce CHD risk is not fully clear**, but may be:
        * proven ability to lower blodo triglycerides
        * likely improved blood pressure and reduced platelet aggregation (=> reduced thrombosis)
        * growing evidence for potent anti-inflammatory actions of n-3 PUFAs

<!--
**Omega-3 fatty acids (Ω-3FA)**  

**Linolenic acid, eicosapentanoic acid and docosahexanoic acid**  

> The group of n-3 PUFA (omega 3 PUFA) includes linolenic acid, eicosapentanoic acid (EPA) and docosahexanoic acid (DHA) . In practice, the term n-3 PUFA is mainly used to refer to the fish oil fatty acids, EPA and DHA. Very little is known about the effect of linolenic acid on blood lipids, whereas there is a ton of information on EPA and DHA.

- _Effects of n-3 PUFA on blood LDL and HDL are (very minor or non-)existent?_
    + First, research showed a 0--40% CHD-decrease when consuming/supplementing n-3 PUFAs, but ...
    + A recent meta-analysis reached the conclusion that n-3 PUFA supplementation is not associated with a lower risk of all-cause mortality, cardiac death, sudden death, and myocardial infarction, provoking heavy criticism from other scientists on the methodological shortcomings and interpretation of the study.
    + By contrast, another meta-analysis concluded that prospective epidemiological studies and interventions provide strong evidence that n-3 PUFA decrease the risk of fatal CHD. 
    + It should be mentioned that **most experts judge the overall evidence in favor of a protective effect of n-3 PUFA on CHD**, whereas **effects on measures of heart dysfunction (fibrillation, arrythmia) are uncertain**. 
    + **`CAVE`**: for reduction in MI-incidence, results are uncertain. Patients surviving their first MI are unlikely to die from a second MI due to extensive pharmacological treatment ((and regular check-ups)). Because of this, 'secondary MI risk reduction by n-3 PUFA consumption/suppletion' is impossible to assess or research, as is any other research into secondary MI risk.
        * This is regarding secondary prevention
    + Primary prevention would be preferable, but researching this in the general public and waiting for the first cardiac event to happen in your intervention (and control) groups would have to include a huge number of subjects (to accumulate enough heart atacks) and will thusly **render it both practically infeasible as prohibitively expensive**.

> Flaxseed and flaxseed oil is often presented as an alternative to fish oil. Flaxseed does not contain EPA or DHA but is rich in (alpha) linolenic acid. This plant-sourced n-3 PUFA is also present in walnuts, chia, echium, and soy. It is often argued that the conversion of linolenic acid into DHA and EPA is very inefficient. That may be true but it doesn’t mean that the conversion is insufficient and would lead to EPA/DHA shortages in people that don’t eat fish.

quite random claim? -- [source](https://edge.edx.org/courses/course-v1:WageningenX+NUTR101+3T2019/courseware/76d1d42cae1a4650a8c4de1189191240/0d54e5354511420e9d156383c0e27477/?child=first)

- **European authorities do not approve of flaxseed and/or flaxseed oil, due to lack of evidence.**

- "_If you eat fish 1x/week, you have a 50% lower risk of fatal coronary heart disease_" -- Dr. D. Kromhout
- 200--400 mg of Ω-3FAs was studied, no effect on cardiovascular effects or on fatal CHD.
    + however, these patients were under poor cholesterol level control (for hypercholesterolaemia)
            - this holds true for healthy individuals, e.g. **for patients post-MI (disproven => due to adequate treatment of patients => cohort study is the answer, but then you cannot say whether the relation is causative)** or DM, Ω-3FA could certainly be beneficial.
    + the effects increase when patients are treated appropriately/sufficiently


>**Dietary fat guidelines**  
> The recommendations for dietary fat in most countries are to limit fat intake to 30-35% of total energy and consume about equal proportions of SFA, MFA, and PUFA. Certain countries recommend to limit saturated fat intake to less than 10% of energy intake. For someone that consumes 2000 Kcal per day that amounts to 22 grams of saturated fat, which equals the amount of saturated fat in about 100 grams of cheese.
> 
> Some of the dietary guidelines in the USA are as follows (2010):
> 
> - Consume less than 10% of calories from saturated fats
> - Replace solid fats with oils when possible
> - Limit foods that contain synthetic sources of trans fatty acids (such as hydrogenated oils), and keep total trans fatty acid consumption as low as possible
> - Eat fewer than 300 mg of dietary cholesterol per day
> - Reduce intake of calories from solid fats
> 
> more at: https://www.eufic.org/en/whats-in-food/category/fats/
-->

- **Dietary fat guidelines**
    + limit fat intake to 30-35% of total energy
    + consume about equal proportions of SFA, MFA, PUFA.
    + limit saturated fat intake to <10% of energy intake (some countries)
- Dietary duidelines **in the USA (2010)**:
    + consume <10% of calories from saturated fats
    + replace solid fats with oils when possible
    + limit foods that contain synthetic sources of trans fatty acids (such as hydrogenated oils), keep total trans fatrty acid consumption as low as possible
    + eat fewer than 300mg of dietary cholesterol per day
    + reduce intake of calories from solid fats

_ _ _ _

### 6. Proteins and Health

#### 6.1 intro

- Proteins
    + chemistry of proteins
    + protein content of foods
    + protein digestion/absorption/synthesis
    + protein functions
    + protein turnover and nitrogen balance
    + protein quality & recommendations
    + potential health effects of proteins

- **After successful completion of this module, you will be able to:**  
    + understand what happens to protein in the body
    + know the approximate protein content of major foods
    + explain how protein is digested and absorbed
    + understand that digestion involves breakdown into amino acids
    + understand why we need protein 
    + understand the concepts of protein turnover and nitrogen balance
    + know the basis for differences in protein quality
    + know the current RDA for protein
    + judge the scientific validity of claims attributed to dietary protein

#### 6.2 Chemistry of proteins

- There are 20 different amino acids. ..
    + .. Nine of these 20 amino acids need to be provided in the diet because the body cannot make these amino acids. They are therefore called **essential amino acids**. .
    + .. The remaining 11 amino acids can be made from the 9 essential amino acids. Any dietary protein provides a mixture of the 20 amino acids in different proportions.


<!-- source: [edX](https://edge.edx.org/courses/course-v1:WageningenX+NUTR101+3T2019/courseware/a55b53c6efcb4e639cb9b4fbf64b4c20/856d91842eb640a196bfe0caef0c103d/?child=first)
 -->

#### 6.3 Protein content of selected foods ####

![prot-food-table](./src/prot-food-table.png)

<!-- ![protein_foods](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/02992e721f568ab23c5ee4c62efe96e7/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/MOOC9.jpg) -->

- **Figure:** The table above provides the protein content of several of the main sources of protein in our diet. The **protein content of real foods never exceeds 25-30%**. The **protein content of protein powders used by bodybuilders can reach 90%**, but before consumption the protein powder must be diluted with milk, water or fruit juice, effectively lowering the actual protein content to below 20%. Excellent protein sources are meat, fish, eggs, and dairy products. Vegans avoid all products of animal origin and therefore have a relatively low protein intake as compared with omnivores. Adult vegans can easily meet their protein requirement by incorporating soy, nuts, beans, and grains in their diet. Raising young children on a vegan diet is not impossible but should be discouraged unless the parents have a solid understanding of the nutritional needs of young children.


#### 6.4 Protein digestion, absorption and synthesis

![prot-metabolism](./src/prot-metabolism.png)

- about 5% of all protein ingested leaves the body intact

<!-- ![protein_dig-abs-synth](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/95ee2f058f8fd73f3e7a2a7b21bcd49c/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/protein_digestion.jpg) -->

- \[Pepsinogen] is produced by the chief cells in the stomach lining, it is converted to \[pepsin] when entering the acidic environment of the stomach lumen.
    + Polypeptide digestion in small intestine via trypsin and chymotrypsin:
        * breaks down long chain polypeptides in to smaller peptides (at specific AA sequences)
    + Peptide digestion further downstream in small intestine via aminopeptidase and carboxypeptidase:
        * breaks down smaller peptides into individual amino acids (exopeptidaes)

<!-- > Protein digestion continues in the small intestine via two major proteolytic enzymes produced by the pancreas named trypsin and chymotrypsin. Similar to pepsin, trypsin and chymotrypsin are produced as inactive pro-enzymes and become activated once they reach the intestine. Trypsin and chymotrypsin cleave the polypeptides arriving from the stomach at specific positions in the polypeptide chain to release smaller peptides. In the small intestine, enzymes produced by the intestinal cells (aminopeptidases and carboxypeptidases) remove single amino acids from both ends of the peptide to finally yield single amino acids. -->

<!-- ![protein_distro](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/ef02f53202e3be7732634bb963faf222/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/protein_synthesis.jpg) -->

> The absorbed amino acids will travel to various organs and tissues to serve as building blocks for the synthesis of body proteins. Certain organs such as liver avidly take up amino acids to synthesize proteins such as the main serum protein albumin. It is estimated that the human body contains more than 100,000 different proteins. Some of these proteins are present in the body in minute quantities, sometimes only in a single cell type, whereas other proteins are very abundant in numerous tissues. Proteins can have a role inside the cells (intracellular) or outside the cell (extracellular). Protein that play a role inside the cell may be involved in production of energy, signal transduction, and maintaining the structure of the cell. Proteins that are secreted by cells may serve as messenger molecule or may be a structural component that keeps cells and tissues together.

#### 6.5 Protein functions (recap)

- Proteins serve multiple functions and function is determined by **amino acid sequence** and the **folding of the polypeptide chain**.
    + persistent faulty incorporation of one incorrect AA in the polypeptide chain can severely disrupt protein function and is at the basis of many genetic diseases
        * osteogenesis imperfecta: very brittle bones that fracture easily
    + **Building material**:
        * many proteins have important sturctural role inside and outside the cell
        * collagen is most abundant in skin, tendons, cartilage, bone, connective tissues
    + **Enzymes**:
        * catalysators for biochemical reactions; every cell has thousands of different enzymes that are responsible for many different reactions
    + **Transporters**:
        * approx. same as enzymes, but with import/export/transport functions across cell membranes.
    + **Hormonnes**:
        * Messengers that circulate the blood. Chemically, hjormones are either polypeptides or steroids.
    + **Antibodies**:
        * Involved in immunce defence against pathogens such as bacteria and viruses.
    + **Regulation of fluid balance**:
        * Water distrubution in the body in compartments:
            - `intravascular <=> intercellular <=> intracellular`


<!-- Proteins serve multiple functions in cells and tissues. The function of a protein is determined by the amino acid sequence and the folding of the polypeptide chain into a specialized three dimensional structure, as shown below. The persistent faulty incorporation of one incorrect amino acid in the polypeptide chain can severely disrupt the function of a protein and is at the basis of many genetic diseases.

- Proteins can fulfill a variety of functions in the body:
    + **Building material**: many proteins have an important structural role inside and outside the cell. The main structural protein and most abundant protein in the body is collagen. Skin, tendons, cartilage, bone, and connective tissue contain a lot of collagen. Collagen acts like a scaffold that gives strength and structure to cells and tissues. Defects in collagen synthesis lead to a severe disease called osteogenesis imperfecta, which is characterized by very brittle bones that fracture easily.
    + **Enzymes**: enzymes are proteins that speed up a biochemical reaction, for instance the conversion of compound A into compound B. In biochemical terms they function as catalysts, which means that they lower the activation energy barrier that needs to be overcome for a reaction to proceed. Every cell has thousands of different enzymes that are responsible for many different reactions, including those that are required to breakdown fatty acids and glucose to generate energy.
    + **Transporters**: transporters assist with import or export of different molecules across the cell membrane.
    + **Hormones**: hormones are messengers that circulate in the blood. They are released from a particular tissue into the bloodstream to signal to distant tissues. For example, insulin is released by the pancreas and travels to muscle and fat tissue to promote glucose uptake. Chemically, hormones are either polypeptides or steroids.
    + **Antibodies**: antibodies circulate in the body and are involved in immune defence against pathogens such as bacteria and viruses. They are also referred to as immunoglobulins and are secreted by plasma cells.
    + **Regulation of fluid balance**: proteins play an important role in making sure that water in the body is appropriately distributed across the bloodstream (intravascular compartment), the space between cells (intercellular compartment), and inside cells (intracellular compartment).
 -->

#### 6.6 Protein turnover and nitrogen balance

- **Protein turnover** refers to the continuous synthesis and degradation of body proteins.
    + => replacement of AAs in proteins ; whether slowly (e.g. structural protreins) or rapidly.
- **Protein balance** refers to the difference between how much protein goes into the body and how much goes out of the body
    + nitrogen (amino-) => urea
    + carbon (-acid) => fuel
        * normally referred to as **nitrogen balance**.
- Normally, human adults are in nitrogen balance (in equals out)
    + Positive nitrogen balance
        * in > out => leads to a net gain of protein in the body (e.g. pregnant, children, bulking)
    + Negative nitrogen balance
        * out > in => net loss of protein in the body (e.g. losing weight, injury, illness, very low intake)

![N-balance](./src/N-balance.png)

- totals 150g of protein
    + 80g of dietary protein a day (avg recommended)
    + 70g of residu proteins is added (from digestive juices, intestinal cells, etc.)
- dietary protein provides considerable energy, amounts to approx 10-15% of total energy intakes
    + Carbon part of some amino acids can be converted into glucose, these AAs are called **gluconeogenic amino acids**.
    + Carbon part of remaining amino acids cannot be converted into glucose, but are converted into ketones, these AAs are called **ketogenic amino acids**.

<!-- 
##### Protein turnover

![protein_synthesis](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/787cb8fc11328a3290fb7700c058573b/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/protein_turnover.jpg)

##### Nitrogen balance

![Nitrogen_balance](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/e0e51b80aa8269c441b3b715f4838fe8/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/MOOC37.jpg)

- "_As indicated above, in individuals that are in nitrogen balance, the amount of amino acids ingested equals the amount of amino acids that is broken down. Breakdown of amino acids generates energy in the form of ATP.  Dietary protein thus provides considerable energy, which amounts to approximately 10-15% of the total energy intake._" -- [source](https://edge.edx.org/courses/course-v1:WageningenX+NUTR101+3T2019/courseware/a55b53c6efcb4e639cb9b4fbf64b4c20/9ab10862405f4877996429538e21af7c/?child=first)
-->

#### 6.7 Protein quality

- Quality of dietary protein is determined by two variables
    + \1. digestibility 
        * proportion of dietary protein that is actually absorbed in the body
    + \2. amino acid composition
        * the higher the similarity in AA composition between a dietary protein and the average body protein, the higher the quality of the protein.
- A meal or diet almost always contains a mixture of different protiens. A mixture of plant proteins has a more favorable AA composition than the individual plant proteins would have alone.
    + e.g. most grains are low in lysine, beans are low in methioninew. By eating beans and grains together, the strenghts of one makes up for the deficiences of the other, creating a complete protein. Combining of protein to achieve higher protein quality is called **protein complementation**.
- **Limiting amino acid**
    + the amino acid that is least abundant in a dietary protein source in comparison to the average human body protein.

<!--
> The quality of dietary protein is determined by two variables:
> 
> - 1) the digestibility (the proportion of dietary protein that is actually absorbed in the body)
> - 2) the amino acid composition.
> 
> The higher the similarity in amino acid composition between a dietary protein and the average body protein, the higher the quality of the protein. In general, plant proteins have a lower digestibility in comparison with animal proteins. Moreover, the amino acid composition of plant proteins is less similar to human body protein as compared with animal proteins. As a result, the quality of plant proteins is generally lower than that of animal proteins.
> 
> A meal or diet almost always contains a mixture of different proteins. A mixture of plant proteins has a more favorable amino acid composition than the individual plant proteins would have alone. That is because the amino acid composition of the mixture of plant proteins more closely resembles human body protein. For example, most grains are low in lysine, whereas beans are low in methionine. By eating beans and grains together, the strengths of one makes up for the deficiencies of the other, creating a complete protein. Combining proteins to achieve a higher protein quality is called protein complementation.

![limiting_AA](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/08c4008b3d955406b1f427918fc8dc5f/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/MOOC10.jpg)

> To enable synthesis of body protein, all amino acids have to be present simultaneously in adequate quantities and proper proportions. A deficit in any one essential amino acid would limit protein synthesis. The limiting amino acid is the amino acid that is least abundant in a dietary protein source in comparison with the average human body protein. For instance, proteins in the human body contain plenty of lysine. In contrast, wheat protein contains little lysine. If we would only eat wheat protein, the conversion of wheat protein into body protein would be limited by the availability of lysine. Therefore, lysine is the limiting amino acid in wheat.
-->

##### Measures of protein quality

- The **Biological value (BV)** is a measure of the proportion of absorbed protein from a food which becomes incorporated into the proteins of the animal.
- The **protein efficiency ratio** (PER) is based on the weight gain of an animal divided by its intake of a particular food protein during the test period. PER used to be the official method of assessing protein quality but has been replaced by **PDCAAS (Protein Digestibility Corrected Amino Acid Score)**.
- The **Amino Acid Score (AAS)** for any particular protein is calculated by comparing the level of the limiting amino acid in the protein in question to the level of the same amino acid in a reference protein. The Amino Acid Score (AAS) for any particular protein is calculated by comparing the level of the limiting amino acid in the protein in question to the level of the same amino acid in a reference protein.
    + The PDCAAS is the AAS corrected for digestibility
- DIAAS (Digestible Indispensable Amino Acid Score) 
    + In PDCAAS, indigestibility is determined by measuring proportion of a particular dietary protein that ends up in stool.
    + In DIAAS, indigestibility is determiend by measuring proportion of a particular dietary AA that reaches the terminal ileum.
        * Rationale behind this:
            - indigestiblity is better determined by measuring at the end of the ileum, in stead of in stool (stool also includes many proteins from endogenous sources)
    + Unlike PDCAAS, DIAAS is not truncated above 100 to a maximum of 100.


- MATH:
    + **PDCAAS** = ( (AA<sub>lim</sub> in 1g of test prot) / (AA<sub>lim</sub> in 1g of ref prot) ) * (prot digestibility (based on prot stool contents) )
    + **DIAAS** = ( (AA<sub>lim</sub> in 1g of test prot) / (AA<sub>lim</sub> in 1g of ref prot) ) * (prot digestibility (based on AA content in ileum) )


##### Recommended protein intake

- RDA for dietary protein is `0.8 g/kg/d` =~ 56g protein / 70 kg adult
    + Most people eat substantially more protein than the RDA, ranging from 10-25% of the total energy intake. 
        * Protein intake >20% of total energy intake can only be achieved by very high consumption of meats, eggs and/or dairy products.
- US dietary recommendation has taken position:
    + healthy adults undertaking resistance or enduramnce exercise do not require additional dietary protein
- **In contrast**, American Dietetic Association, Dietitian sof Canada, American College of Sports Medicine:
    + recommend protein intake 1.2-1.7g/kg/d for endruance and strength trained athlethes.

- Dr. Stuart Philip and Dr. Luc van Loon have stated in their paper in the _Journal of Sports Sciences_ in 2011 that protein may do more than just supply building blocks for protein synthesis
    + dietary protein (and espj. the amino acid leucine) may be important for enhancing protein-mediated recovery and adaptation.
    + .. they also state that:
        * _"the optimum timing for protein ingestion to promote the most favourable recovery and adaptaion is after exercise"_

##### Health effects of dietary protein

- **(Unproven) Health Effects**
    + no conclusive evidence linking protein to:
        * CVD
        * cancer
        * osteoporosis
        * .. despite claims in media
    + no conclusive evidence for a relation between:
        * protein intake and energy intake and bodyweight
    + high protein intake should be avoided by patients with kidney disease

- Peterson et al. made a careful overview (systematic literature review):
    + **none of the evidence linking dietary protein to a certain outcome could be classified as convincing**
    + classified as: **probable to convincing**
        * inversed relationship between soy protein intake and LDL cholesterol
    + classified as: **suggestive**
        * inverse relationship of total and animal pjrotein intake with increased risk for DM2
        * inverse relationsihp between vegetable protein intake and cardiovascular mortality
        * inverse relationship between vegetable protein and blood pressure
        * an effect of physical training on whole body protein retention
    + classified as: **inconclusive**
        * impact of physical training on protein requirement
        * relationship between protein intake and:
            - cancer mortality
            - cancer diseases
            - bone health
            - energy intake
            - body weight
            - body composition
            - renal function
            - risk of kidney stones
            - CVD
            - all-cause mortality
- Scientific evidence to support specific health claims of dietary protein is very limited and weak.
- General differences between plant and animal protein:
    + it is **highly unlikely** that the different effects of animal vs plant protein can be explained by differences in AA composition or digestibility
    + it is also **unlikely** that plant proteins as a whole have different functional properties than animal proteins.

<!--
##### Health effects of dietary protein

- More protein (whether animal or plant) consumption is non-conclusive for
    + link to CVD, cancer, osteoporosis
    + no relation between protein intake and energy intake and m<sub>body</sub>
    + high protein intake should be avoided by patients with kidney disease
-->

<!--
**Dietary protein and muscle size and function**  
For many years, bodybuilders and other strength athletes have been consuming a diet high in protein. Indeed, since the sport first emerged in the late 1800’s, high-protein foods such as eggs, fish, and meat have formed the basis of most bodybuilder’s diet. In the last decade, the scientific evidence to support such practices has grown substantially. It is now well accepted that a high protein diet can stimulate muscle mass in individuals that engage in resistance-type exercise. Studies have shown that the most optimal moments to ingest protein are shortly after exercise and just before going to bed. The consumption of protein before going to bed helps to mitigate the catabolic state and accompanying muscle breakdown that inevitably develops during the sleeping period.

> the combination of resistance exercise and the extra protein is a very effective strategy to combat sarcopenia and slow down the age-related decline in muscle mass and functionality. A reasonable target for daily protein intake is 1.6 g/kg/day. The long-term consequences of such a high protein intake remain unclear.

source: [edX](https://edge.edx.org/courses/course-v1:WageningenX+NUTR101+3T2019/courseware/a55b53c6efcb4e639cb9b4fbf64b4c20/d86723c88d0c40ed9294d62349c69507/?child=first)
-->

- Interest in potential impact of extra protein in older adults
    + aging is associdated with gradual decline in muscle mass and concomitant increase in fat mass (condition referred to as _sarcopenia_)
        * many older adults consume a **diet relatively low in protein** AND
        * consuume most of the protein during a **relatively narrow time window**
    + Studies (at Wageningen) have shown that **ingestion of extra protein outside this time window can increase muscle mass in older adults engaging in resistance exercise**
    + **Resistance exercise has been shown to effectively increase muscle strength and functionality in young and old**.
        * Acordingly, **combination of resistance training and extra protein** is a very effective strategy to **combat sarcopenia** and **slow the age-related decline** in muscle mass and functionality.
    + A reasonable target for daily protein intake is `1.6g/kg/day`, but long-term consequences of such a high protein intake remain unclear.

_ _ _ _

### 7. Energy homeostasis and energy balance

#### 7.1 intro

- Energy homeostasis & balance
    + Regulation of food intake
    + Energy value of nutrients
    + Energy expenditure
    + Weight gain & loss

#### Learning outcomes

- understand the basis for weight gain and loss
- explain the concept of energy balance
- understand the basic mechanisms involved in regulation of food intake
- understand the caloric value of foods as expressed on a food package to understand how the energetic value of nutrients is calculated
- estimate the rate of energy expenditure for various activities
- know to importance of maintenance (basal metabolic rate) in determining energy expenditure
- explain why weight loss and gain automatically level off

#### 7.2 Energy Balance

- Three macronutrients
    + carbohydrate
    + fat
    + protein
- Energy balance is often considered as:
    + _the balance between total energy intake and energy expenditure_
    + strictly, this is not correct:
        * energy expenditure is not the only way by which the body loses energy
        * some energy is lost in stools and urine

![energy-balance](./src/energy-balance.png)

_Figure_: left: positive energy balance, right: negative energy balance

#### 7.3 Regulation of food intake

- **Hunger** is defined as the uncomfortable sensation caused by lack of food that makes people look for food
- **Appetite** is the integrated response to the sight, smell, thought or taste of food that triggers eating.
    + Sometimes appetite is driven by hunger, but often it is due to cravings, habits, availability of food, boredom, other socio-emotional factors.
    + Hunger is a very dominant sensation that impairs a person's ability to focus on others tasks. People **can have appetite without being hungry** (= normal situation); people can also **have no appetite for food even though you are hungry** (= stressfull situation, illness)
- **Satiation** is oftend defined as the feeling of satisfaction and fullness that occurs **during a meal** and that **makes you stop eating**.
    + Satiation determines how much food is consumed during a meal.
    + It is your immediate reaction to th eingestion of food.
- **Satiety** is defined as the feeling of satisfaction that occurs **after a meal** and **prevents you from eating until the next meal**.
    + colloquially, these two terms are often used interchangeably

- Foods that promote satiation are generally:
    + very bulky
    + have high water content
    + have high fibre content
    + cary strong sensory attributes
- It is important to realize that the satiating properties of a food are not just determined by its nutrient content.
    + Texture is also very important:
        * e.g. There is evidence that eating an apple is more satiating than drinking apple juice, even if that apple juice was prepared in a blender using the entire apple. ..
        * .. It has been observed that consuming energy-containing beverages, which are ingested very quickly, does not make people eat less of other foods therafter, perhaps because calories that are ingested quickly are not properly sensed.
        * Prolonging exposure time to foods (chewing) causes people to quit eating sooner and consume less energy in total.
- It is important 

#### Factors determining food intake

- Sensory factors
    + sensory properties of a food (taste, smell, sight, sometimes sound
        * believed to play major role in food intake
- Social factors
    + much of what we eat is determined by our culture.
    + different cultural backgrounds often have entirely different culinary customs
    + social factors also include social setting
        * we tend to eat more in the company of others than when we eat alone
    + it is clear that social factors can easily override physiology (at least in the short term)
- Psychological factors
    + Many people experience psychological stress, which can have a major impact on someone's food intake. 
        * loss of appetite vs.
        * food intake increase ('stress eating', 'emotional eating')
- Physiological factors:
    + Our desire for food is driven by the evolutionary need to supply our bodies with nutrients and energy so we can survive, propagate, pass on our genes.
    + As a result, our bodies have evolved feedback mechanisms, that lead to hunger sensations and trigger food seeking behaviour when food intake and energy supplies drop.

#### Feedback regulation of food intake

- Physiological regulation of food intake is mediated by:
    + **short term feedback**
        * originate from Tr. Dig and influence satiation and satiety.
    + **long term feedback**
        * originate from adipose tissue and relfect the size of the adipose tissue stores
    + The signals are received in the hypothalamus and integrated with other stimuli that influence hunger, appetite and food seeking behavior (i.e. emotional-, cognitive- and hedonic stimuli)
        * Food intake will cause immediate distension (stretching) of the stomach that conveys the feeling of fullness.
        * Entry of food into teh Tr. Dig will inhibit release of hunger hormones and stimulate the release of satiety hormones.
            - Well known satiety hormones include **CCK (cholecystokinin)** and **GLP-1 (glucagon-like peptide 1)**
            - Entry of glucose and AAs into the blodostream activates nutrient-sensing nerve cells in the hypothalamus

![feedback-reg](./src/feedback-reg.png)

- **Leptin**
    + Long term regulation occurs via leptin, which is produced by fat tissue.
    + **Production and blood levels of leptin are proportional to the amount of fat mass**
        * Lack of laptin due to genetic defect causes extreme hunger, massive overfeeding and obesity.
        * In many humans, despite ample leptin production, bodyfat tends to increase with advancing age (compared to many animal species in the wild, which maintain a relatively stable body weight).
- **Hunger**
    + Decrease in leptin production trigger hunger cells in the hypothalamus, leading to strong hunger sensations.

#### Feedback regulation from adipose tissue

![feedback-adipose](./src/feedback-adipose.png)

- adipose tissue plays a central role in **long term regulation** of food intake.
    + `bodyfat gain => leptin release from fat tissue => stimulate satiety cells in hypothalamus => reduction in food intake => bodyfat loss`
    + `bodyfat loss => decreased leptin release form fat tissue => stimulate hunger cells in hypothalamus => increase in food intake => bodyfat gain`
- The reduction in leptin levels during weight loss is beleived to be one of the reasons why people have difficulty maintaining weight loss.

#### 7.4 Energy value of nutrients

- Dietary energy is provided by three macronutrients
    + carbs (30-70%)
    + fat (15-50%)
    + protein (10-25%)
- Different types of energy
    + **Gross energy (GE)**
        * includes all the energy in food product
        * it is not all available to the animal
        * it is determined by how much heat is liberated upon combustion
    + **Digestible energy (DE)**
        * the amount of energy that is absorbed
        * it is dpeendent on the digestiblity of a food, which is normally considered constant
            - 98% for carbs
            - 95% for fat
            - 92% for protein
    + **Metabolisable energy (ME)**
        * what is left after accounting for energy in feces and urine
        * it is **what we use to calculate the energy content of food based on macronutrient content** (as written down on a food package)
- **Atwater factors**
    + 4 kcal/gram for carbohydrates
    + 9 kcal/gram for fat
    + 4 kcal/gram for protein
    + it is important to realize that the Atwater factors (and thus the calculation of energy content of foods) makes assumptions about digestiblity and the amount of heat liberated upon combustion (see also: _above_)


<!-- ![Flow_chart_of_energy](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/13792938f259af4f1ab42d64901de188/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/MOOC13.jpg) -->


|      | GE KJ/g | DE kJ/g | ME kJ/g | ME kcal/g |
|------|---------|---------|---------|-----------|
| carb |    17.6 |      17 |      17 |         4 |
| fat  |    39.8 |      38 |      38 |         9 |
| prot |    23.6 |    21.9 |      17 |         4 |
| EtOH |     290 |    29.0 |      29 |         7 |

- Energetic value of fibre
    + USA: for calorie calculation, the amount of insoluble fibre can be substracted from total carb content. Therefore the metabolisable energy value assigned to **insoluble fibre** is 0 kcal/g and the energy value for **soluble fibre** is 4 kcal/g
    + In most countries, including the EU, the energy value assigned to fibre is 2 kcal/g (without differentiating between soluble and insoluble fibre)
- Metabolisable energy
    + what is left after accounting for losses in feces and urine
        * similar to a car or power station, a portion of energy combusted is lost as heat
        * remainder of the energy is converted to ATP = **Net energy** (see also: _flowchart_ in 7.4)
    + Net Energy is directly available for performing various tasks in the body

- **Dietary thermogenesis**
    + dietary induced thermogenesis describes the transient increase in energy expenditure following meal consumption caused by digestion, absorption and processing of the nutrients.
        * it has been observed that dietary induced thermogenesis is higher for protein than for fats and carbohydrates ..
        * .. which means a larger share of the calories consumed as proteins are needed to process the dietary protein as compared with fats and carbohydrates

#### 7.5 Expenditure of energy

#### Flow chart of energy

![Flowchart-of-energy](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/13792938f259af4f1ab42d64901de188/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/MOOC13.jpg)

- ad. _Flowchart of energy_: the total amount of energy coming in (gross energy) is equal to the energy going out.
    + **Energy going out** (2 components):
        * \1. energy lost in stools and urine
        * \2. energy lost as energy burned (expenditure)
        * `CAVE:` energy content of foods and calculation of our energy intake is based on _metabolisable energy_, which means that **loss of energy in stools and urine has already been taken into account in the calculation**
    + **Net energy**
        * available energy for performing various tasks in the body (3 categories):
            - \1. maintenance
            - \2. physical activity
            - \3. growth

#### Maintenance

- Maintenance are those physiological processes that are essential for the survival as an individual, e.g.:
    + heart beat
    + brain function
    + respiration
- Energy required for basal maintenance is expressed as **Basal Metabolic Rate (BMR)**. It is measured in subjects that are:
    + fully fasted
    + at a comfortable temperature
    + awake, but completely at rest
- Besides BMR, maintenance includes obligatory energy costs of digestion and absorption (also called dietary-induced thermogenesis; see also: _above_).
    + Dietary-induced thermogenesis is about 10% of the caloric value of a mixed meal => this means that **10% of the energy we consume in a meal is already expended in the digestion and absorption of the meal**
- BMR covers about 60-70% of our daily energy expenditure
    + BMR depends on individual and lean body mass (`mass of the body - the fat`).
    + BMR can be estimated using different **formulas**. The best known formula is from **Harris-Benedict**:
        * BMR,M = `66.5 + (13.75 ∙ m) + (5.003 ∙ l) - (6.775 ∙ age)`
            - m = body mass in kg ; l = height in cm ; age = age in years
        * BMR,F = `655.1 + (9.563 ∙ m) + ( 1.85 ∙ l) - (4.676 ∙ age)`
            - m = body mass in kg ; l = height in cm ; age = age in years
    + .. alternative is **Mifflin-St Jeor** formula:
        * BMR,M = `(10 ∙ m) + (6.25 ∙ l) - (5 ∙ age) + 5`
            - m = body mass in kg ; l = height in cm ; age = age in years
        * BMR,F = `(10 ∙ m) + (6.25 ∙ l) - (5 ∙ age) - 161`
            - m = body mass in kg ; l = height in cm ; age = age in years

#### Physical activity

- **Physical activity** represents the most variable ocmponent of energy expenditure
    + from 100s of calories in the sedentary to 1000s of calories in competetive endurance athlethes
- **Physical Activity Level (PAL)** is defined as total energy expenditure divided by the **BMR**.
    + most people have a `PAL = 1.6-1.7`
    + the physically very active have a `PAL > 2.0`
    + '' extremely inactive have a `PAL < 1.4`


![energy-expend-mass](./src/energy-expend-mass.png)

- ad. _Energy expenditure by body mass_: rough estimate of energy expended in various types of exercise
    + `CAVE:` please realize that even though running burns up the most calories per hour, the more contracted duration of a typical running session limits the total energy expended when compared with for instance cycling at a moderate pace, which many people can do for hours.

- **Physical Activity Ratio (PAR)** is another measurement of exercise intensity (in addition to kcal/h).
    + **PAR is the energy cost of an activity expressed as a multiple of BMR**
        * activities such as lying, standing, sitting at rest, watching TV: `PAR = 1.0-1.4`
        * walking at a moderate pace: `PAR = 4.0`
        * vigorous activities, running, cross-country skiing: `PAR > 10.0`

- **Indirect calorimetry**
    + uses measurement of the rate of O2 consumption (and sometimes CO2 production) to determine the rate of energy expenditure.
    + Different systems exist, but all rely on:
        * \1. O2-concentration in the expired air
        * \2. volume of air expired per minute
    + Rate of oxygen consumption (l/min) can easily be converted into endergy expenditure (in kcal/min) by multiplying by 4.8 (energy equivalent of one liter of O2).

#### 7.6 Weight gain and loss

#### Weight gain

- if `energy intake > energy expenditure ; energy expenditure => bodyfat (m↑)`
    + when `energy expenpenditure > 600 kcal`
    + then `energy expenditure => (m↑) levels off`
    + because `Δenergy expenditure = Δm`
        * so `intake↑ = m↑`

![weight-gain](./src/weight-gain.png)

> <u>**example**</u>  
> Let’s examine what happens in a 60 kg actress that needs to gain about 10 kg for a role in a new movie. She decides to increase her daily energy intake (currently at 1800 kcal/day) by 600 kcal. It is estimated that one kg of weight gain (fat gain) represents 7500 kcal of caloric excess. Accordingly, to gain 10 kg of bodyweight she would need to accumulate a caloric excess of 75,000 kcal. Because her daily caloric excess is 600 kcal, she theoretically would need 75,000/600=125 days to gain the desired 10 kg. What will happen, though, is that the caloric excess of 600 kcal will gradually diminish, as her resting metabolic rate and total energy expenditure will progressively increase, unless she compensates by further raising her energy intake. Her weight gain will thus slow down and she will need probably closer to 225-250 days to gain the necessary 10 kg. Or she may not even achieve her aim and reach a new stable bodyweight at 68.5 kg consuming 2400 kcal/day. 

- Why `intake↑ = m↑`? 2 main reasons:
    + \1. The **BMR** will increase as bodyweight increase. People gain fat but also lean body mass as they gain weight. The BMR is mainly determined by the amount of lean body mass.
    + \2. The energy cost of physical activity will be higher with greater bodyweights. __*__  Lifting up your arm, walking, maintaining posture all take more energy as the weight6 of the limbs and overall body mass increases

__*)__ `m↑ => energy expenpenditure↑`

#### Weight loss

![weight-loss](./src/weight-loss.png)


><u>**example**</u>  
> The opposite happens when the actress wants to return to her previous bodyweight of 60 kg by going back to her old eating habits that provided 1800 kcal/day. Initial weight loss is rapid but progressively slows down as the energy deficit (the difference between energy expenditure and energy intake) becomes smaller. The reason for that is as she keeps losing weight, her energy expenditure will decline as well due to a decrease in basal metabolic rate and energy requirement for physical activity.  
> <u>ad **example**</u>  
> The above scenario assumes that the actress will be able to stick to the 1800 kcal/d during the weight loss period and thereafter. It is possible that, having subsisted on 2400 kcal/d for a while, the actress finds it increasingly difficult to stay on 1800 kcal/d and ends up eating 2000 kcal/d. In that case, she won’t be able to return to her previous 60 kg and will stabilize at 62.5 kg, all assuming that she doesn’t change her activity pattern.      


_ _ _ _

### 8. Weight management

#### 8.1 Introduction to weight management

- Weight management
    + assessing obesity
    + health risks of obesity
    + causes of obesity
    + possible solutions for obesity

- Learning outcomes
    + know the criteria for overweight and obesity
    + recognize the large diffrenc ein obesity rates between countries
    + know the major diseases for which obesity is a risk factor
    + understand that obesity is multifactorial and that the approach to tackle obesity is dependent on the individual and surroundings
    + recognize the various approaches to achieve weight loss
    + understand and appreciate that everybody should ultimately figure out what works for them (personalised nutrititon)

#### 8.2 Assessing obesity

##### Definitions of obesity

- **Body Mass Index (BMI)**
    + normal: `20 > BMI < 25`
    + overweight `25 > BMI < 30`
    + obese `30 > BMI 35`
    + morbidly obese `BMI > 40`
    + superobese `BMI > 50`
- Disadvantages:
    + BMI is based entirely on body mass and height and does not take into account body composition.
    + e.g. athlethes carrying lots of muscle, but little bodyfat may be erroneously classified as overweight, based on BMI alone.

- **Fatfold or skinfold tests**
    + measuring skin and underlying subcutaneous fat layer at specific sites:
        * front thigh
        * upper back
        * waist
        * upper arm (tricpes)
    + based upon assumption that there is a strong correlation between thickness of subcutaneous fat layer and total amount of bodyfat
- **Hydrodensitometry/air displacement plethysmography** (underwater weighing)
    + measures volume of the body => dividing body mass by body volume yields the denisty of the body. This can be used to compute body fat percentage.
    + **special equations assume densities**
        * for fat mass of 0.9
        * for lean body mass of 1.1
    + **Bioelectrical impedance**
        * by sending tiny electrica impules through the body and measuring return of those impulses
            - based upon different tissues possessing different conductive properties:
                + less resistance is correlated with a leaner physique
                + more resistance is correlated with a fattier physique
    + **Dual energy X-ray absorptiometry (DEXA)**
        * uses x-rays to determine body composition
        * very accurate
        * yields other relevant information (incl. bone density)
        * very expensive and risks because of x-ray exposure

- non-obese adult males have a bodyfat percentage between 10-20%
- non-obese adult females have a bodyfat percentage between 20-30%


##### Body fat distribution

- Most bodyfat is stored below the skin (=**subcutaneous fat**)
- A small, but clinically relevant, amount of body fat is stored within the abdominal cavity (**intra-abdominal/visceral fat**), connected to:
    + intestine
    + pancreas
    + other organs ..
- Body fat distributions are often categorized as
    + _apple shaped_ = more intra-abdominal/visceral fat (central/abdominal obesity) ; typically associated with males
    + _pear shaped_ = more subcutaneous fat (more fat around buttocks and legs) ; typically associated with females

![bodyfat-distr](./src/bodyfat-distr.png)

> ad. _Fig_ The picture shows an MRI image of the abdominal area of two individuals with equal BMI but that carry vastly different amounts of visceral fat. The outer white layer depicts the subcutaneous fat layer. The white parts within the dark inner circle is the visceral fat. It is obvious that the individual on the left carries little visceral fat whereas the individual on the right has lots of visceral fat. The difference in visceral fat is reflected in the waist circumference, which is higher in the individual on the right.

- The amount of visceral fat is relatively well correlated with waist circumference or waist to hip ratio (ratio of waist circumference to hip circumference), which can be much more easily assessed using a tape measure.

#### Global obesity prevalance


![global-obesity](./src/global-obesity.png)

> ad. _Fig_ Obesity rates differ a lot between countries. According to the WHO, the highest obesity rates are found on the Pacific Islands. People on the Pacific Islands not only consume an energy-rich diet but also seem to have a genetic predisposition towards obesity. Other countries where obesity is very common include the US and Australia. Obesity rates are substantially lower in most European, African, and Asian countries. The lowest obesity rates are found in poor African countries such as Ethiopia and Eritrea. **Numerous surveys have shown that more and more people are suffering from obesity**. This trend does not just affect Western countries but almost any country in the world. Why obesity rates have been rising so fast will be discussed later on.


#### 8.3 Health risks of obesity

##### BMI and disease risk

- Obesity is associated with an increased risk of death
    + increases risk of DM2
    + CVD risk increases
    + sleep apnea
    + CVA/stroke
    + pancreatitis
    + (osteo)arthritis
    + gallstones (cholelithiasis)
    + cancer (breastca and colonca)
    + reproductive problems


##### Metabolic syndrome

- abdominal obesity
- hypertension
- dyslipidaemia: elevated blood cholesterol and triglycerides
- insulin resistance
- ( also consider chronic low-grade inflammation of fat mass/fat tissue in obese individuals )

#### 8.4 Causes of obesity

![84-2](./src/84-2.png)
![84-2](./src/84-2-dieting.png)
![84-3](./src/84-3-diets.png)
![84-4](./src/84-4.png)
![84-5](./src/84-5-dietfats.png)
![84](./src/84-obesity-energybalance.png)
![84](./src/obesogenic.png)
![85-bariatric](./src/85-bariattic.png) ![85-PE](./src/85-PE.png)

#### 9

##### Interview transcript

Common questions and answers
Below you can find a selection of questions that were raised on the discussion forum. The answers are provided below each question.

Q: there is a lot of activity going on related to ketogenic diets. They are claimed to confer numerous health benefits. What is ketosis and how does the Nutritional science community look upon this matter?

1) What is the mechanism of ketosis? - Ketosis was not discussed in this course, although fat metabolism was part of the curriculum.

2) Is it extra effective in weight loss (ketones are excreted in the urine)? - Excretion of energy in the form of ketones via urine should subtract energy from metabolizable energy which thus can't be stored as fat.

3) Which parts of the body burn ketones? Can the brain burn ketones? - Muscles burn glucose, glycogen and fatty acids, the brain burns glucose, what role do ketone bodies play? Ketones apparently can pass the blood brain barrier and supposedly be burned in the brain. I'm really interested if this has any merit, if it is true or false?

4) Can it be harmful to the body? - Acidosis (in which the body produces ketone bodies) is a harmful state in diabetes patients, isn't it in a diet regarding fat and health?

A: Ketosis describes the formation of ketone bodies. There are three ketone bodies: acetone, beta-hydroxybutyrate and acetoacetate. They are made from fatty acids in the liver when the supply of fatty acids to the liver is greatly elevated. An example is (prolonged) fasting. Another example is uncontrolled diabetes. Normally, concentrations of ketone bodies in the blood are very low. During my post-doctoral training at the University in Lausanne in Switzerland I studied ketosis and discovered the mechanism responsible for activation of ketone body formation during fasting. My greatest scientific contribution so far !

Another way to raise ketone body formation is by following a so called ketogenic diet. The main feature of ketogenic diets is the extremely low carb content. Some of the ketone bodies end up in the urine, which thus represents a net loss of energy from the body. The concentration of ketones in the urine can easily reach 0.5 mM but can achieve far higher levels when combined with exercise. 

Ketone bodies can be used by many organs. The liver produces ketone bodies but cannot use them. It just exports the ketone bodies for use by other organs. The brains can use ketone bodies but not immediately. Normally, the brain is fully dependent on glucose. During prolonged fasting, the brain progressively acquires the ability to use ketone bodies as fuel. I assume a similar thing happens when switching to a ketogenic diet. The muscles also use a lot of ketone bodies when they are available.

The evolutionary advantage of ketone body formation is during fasting. The challenge that the body faces during prolonged fasting is to supply glucose to the brain. Since we only have limited glucose stores as glycogen, the body needs to be break down valuable muscle protein to provide amino acids, which in turn can be converted into glucose. This is what happens during the first couple of days of complete fasting. This situation cannot be sustained very long because of the rapid muscle loss. The solution that the body has come up with is to take advantage of the abundant fat stores. However, the brain cannot use fatty acids as fuel (has nothing to do with blood-brain barriers, that's nonsense). What happens instead is that the liver takes up the fatty acids and converts them into ketone bodies, thereby generating fuel for the brain. In this way, stored fat can be (indirectly) used by the brain. The need to degrade muscle protein is reduced and survival extended.

Q: At what point does an increased level of ketones start to cause problems in the body?

A: Problems with ketones only occur during uncontrolled diabetes (diabetic keto-acidosis) and alcohol abuse (alcoholic keto-acidosis). The high levels of ketones in the blood lower the blood pH (more acid) to the point where it can be life threatening. This doesn't happen during fasting or consumption of a ketogenic diet. Levels of ketones are nearly undetectable after a carbohydrate-containing meal and can rise to 0.5-1 mM after a one day fast. Levels can reach over 6 mM after prolonged starvation. Levels of ketones in the blood can exceed 20 mM during diabetic keto-acidosis.

Q: I have few questions about storage of the glycogen. After a heavy meal, for instance:

Is it true that the storage of glycogen in liver will be filled up first, then the "redundant" glycogen will be transported to muscles?

A: No. The liver takes up the leftover glucose from the blood that the other tissues don't need or can't process. First the brain and other tissues take out their share, and the leftovers go to the liver. In scientific language, glucose is prioritized to extra-hepatic tissues. It means that muscle glycogen will be filled up first, followed by liver glycogen (assuming that they are both empty, which is rare. For instance, after heavy exercise in the morning without having eaten breakfast). Glycogen cannot be transported in the blood. Glucose can.

Q: How could I know if the storage in liver and muscles are filled up?

A: Whether your muscle and liver glycogen stores are filled up depends on the composition of your diet, prior exercise, and the time since your last meal. Your muscle glycogen stores should be mostly filled up unless you just did (intense) exercise or your habitual diet is very low in carbs. The level of liver glycogen depends on whether you just ate. The more time since your last meal, the lower your liver glycogen levels. Liver glycogen will be fully depleted after about one day of complete fasting. As for muscle glycogen, consumption of a very low carb diet will minimize glycogen storage in liver.

Q: We learned that "almost all the fructose is cleared in liver, so no fructose reaches the tissues outside liver". What does the "clear" mean? Does liver treat the fructose as same as the glucose, i.e. convert the fructose to something like glycogen for storage? And if we over-consume fructose (eat really a lot of fruits for instance), will liver also convert the redundant fructose to fat and store it in/around liver?

A: Clear means remove or process. All dietary fructose is processed by the liver. The other tissues lack the enzyme to handle fructose. In addition, the liver already removes nearly all the fructose which means very little gets to the rest of the body. This is not the case with glucose. The liver may process some but a lot goes elsewhere (brain, muscle, other tissues). The concentration of fructose in the blood (about 8 micromoles/L) is nearly a thousand-fold lower than the blood concentration of glucose (about 5 millimoles/L).

In the liver fructose can be converted into glycogen. But if glycogen stores are full it will be converted into fat. The fat can be exported from the liver as part of VLDL or it can stay in the liver.

Q: Does that mean that compared to eating starch, eating a lot of fruits will increase the risk of having more fat in the liver? (Because only liver can process fructose). Will our body consider something like "transportation cost", i.e. prefer to store the fat nearby the production site?

A: Well, I wouldn't go that far. The carbohydrate content of fruits is quite low and only part of it is fructose. There is some evidence that massive overconsumption of fructose causes fatty liver but it would be hard to reach those levels eating fruits, although it may easier by consuming fruit juices (because you tend to consume more of it). It is amazing what some people do. Some people drink 2 liters of orange juice per day because they perceive it as healthy. Clearly, anything in excess is potentially harmful, including fruit juices.

Our body prefers to store the fat where it belongs: in our fat tissue. However, when there is too much body fat, some excess fat may end up elsewhere, including in the liver.

Q: What happen of fructose processed in liver? It will turn into energy or just a waste?

What makes us unhealthy: weight gain (obesity) or added sugar?

What is the purpose of Isocaloric replacement?

If sugar and HFCS consist of approximately equal amounts of fructose & glucose, why do we still need HFCS? Given that HFCS undergoes several enzymatic steps to produce, the cost should be higher.

A: Fructose will be processed in the liver. Depending on your nutritional status (did you just eat a meal or fast overnight) the fate of the fructose will be different. After a substantial period without food, fructose will be used to make liver glycogen, or be converted into glucose and released as such. In the fed state it will be used as an energy source and it may also be converted into fat. There is a lot of controversy about this issue.

I think the weight gain is the primary problem. The current evidence that high sugar intake promotes type 2 diabetes in the absence of obesity is not so convincing, in my view.

In research we prefer isocaloric replacements because the comparison becomes unfair if you don't (you end up with difference in weight gain).

It is financially attractive to make HFCS in countries that produce lots of corn (which may be related to subsidizing, this is not my area of expertise). In the Netherlands, corn is used to feed cows. We produce sugar from sugar beets.

Q: I ran across the citation for this article linking non-caloric artificial sweeteners, changes in gut biota and glucose intolerance. Nature is a reputable journal and this is a recent article, but I can't read the actual article without paying for it ($32US!) so it is difficult to find out the details of the study.

A: The paper in Nature generated a huge amount of publicity but also criticism. They were large limitations in the study designs, especially for the human studies which were very questionable. I think very few professionals in the field are willing to embrace the concepts presented in this paper. The data were too poor and the whole idea is simple not realistic. We are currently living in an era where linking anything to the gut microbiota will get your paper published in high impact journals. It actually is quite disturbing.

Q: I often have a banana, kale, oat, almond butter smoothie for breakfast. This concoction seems like it has a pretty good amount of fiber (especially if the banana is a bit green, I've just learned!) BUT, I wonder if I am decreasing the fiber content by vigorously blending everything up or if those indigestible fibers will make it to the colon anyway.

A: Whether you pulverize the fruits and vegetables in your mouth or in a blender, it does not make any difference. The fiber molecules will remain completely intact.

Q: In the multiple choice question 3.2 regarding Health Effects of Sugar there is a question saying "There is clear evidence from studies in humans that dietary fructose promotes fatty liver". By doing a tenth of a second search on this topic, I found the following study: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2673878/

It is stated that "These data suggest that dietary fructose specifically increases DNL, promotes dyslipidemia, decreases insulin sensitivity, and increases visceral adiposity in overweight/obese adults." ...where DNL being hepatic de novo lipogenesis. Tons of other studies suggest the exact same - that fructose promotes a fatty liver in both animals and humans. Is it a fault in the test or are there some scientists at the university blinding themselves - maybe due to corporation with the food industry?

A: A large number of animal studies have indeed found a stimulatory effect of dietary fructose on liver fat. We also find that putting fructose in the drinking water of mice leads to a fatty liver. The number of studies in human subjects is still limited and therefore the overall evidence is (still) inconclusive. The study you are referring to received lots of publicity but did not measure fatty liver (they measured lipogenesis, which describes the production of fatty acids from glucose). Therefore, it cannot be concluded from this study that fructose promotes fatty liver. An important issue is the amount of fructose that is provided, as pointed out in a recent article by Dr. Barbara Fielding from Oxford University and the University of Surrey. She concluded: "...that the use of hypercaloric, supra-physiological doses in intervention trials has been a major confounding factor and whether or not dietary sugars, including fructose, at typically consumed population levels, effect hepatic lipogenesis and NAFLD pathogenesis in humans independently of excess energy remains unresolved." See; http://www.ncbi.nlm.nih.gov/pubmed/25514388

From a mechanistic point of view it would make perfect sense that a high intake of fructose would promote fatty liver as fructose is entirely metabolized in the liver, which is different from glucose. But that's not enough. We need properly designed human trials to resolve this question.

Q: What is the deal with a leaky gut.

A: A lot of the stuff you read about on this topic is very speculative and basically represents misinterpretation of scientific data. The leaky gut is a valid concept in the context of GI diseases including GI infections where there could be transfer of bacteria across the intestinal wall. Currently, there is a lot of research going on to investigate the role of the gut bacteria in many human diseases. The idea is that "bad" bacteria in the intestine release certain compounds that are taken up into the body and do harm. The field is evolving quickly and some claims are becoming reasonably solid whereas others remain very hypothetical and weak. Also, it seems that nearly every human disease has now been linked to the gut bacteria which can't be right.

In a few years it may well turn out that we accept that the gut bacteria are key to many human diseases but there is too much noise in the scientific literature to be very confident at this stage.

Q: While studying table "Concentrations of macronutrients in food" I'm interested in how Scientists measure amount of macronutrients in particular food? What approach they used to do this?

A: It is done via chemical analysis.

The traditional method for determining the fat content of a food is via solvent extraction. A weighed portion of a food is mixed with a mixture of chloroform and methanol (an alternative method uses ether). Further dilution with chloroform and water separates the mixture into two layers, the chloroform layer containing all the lipids and the methanolic layer containing all the non-lipids. The chloroform layer is taken and the chloroform is allowed to evaporate, leaving the fat behind, which can be weighed.

Protein content is determined via a different method called the Kjehldahl method. The method determines the amount of nitrogen in a weighed portion of a food, which can be converted into protein using a conversion factor.

The traditional method for determining carbohydrate is by difference. Total weight minus fat weight minus protein weight minus ash (minerals) minus water content gives the carbohydrate content. Nowadays, additional analytical methods are performed to determine the various types of carbohydrate present in a food.

There are several ways to determine the fiber content of foods. Fiber content is deducted from total carbohydrate to yield the available carbohydrate content. For more info see: http://www.fao.org/docrep/006/y5022e/y5022e03.htm

Our laboratory at the Division of Human Nutrition at Wageningen University routinely does these types of analyses to determine the exact composition of a diet that was given to people as part of a dietary intervention. Basically, we prepare two plates of the same meal. One plate is given to the participant of the study, the other plate is dumped into a bin. This is done for every meal. Afterwards, the content of the bin is ground into a paste (nasty job...) which is then analyzed for the macronutrient and/or micronutrient content. That way we know exactly what the study participant ingested assuming that they cleaned their plate, which is mandatory.

Q: If a person was stranded with no food and only water, what breaks down into the glucose for them to stay alive?

A: It is mainly amino acids originating from muscle. Muscle protein will be broken down to amino acids, which will be used to produce glucose via a process called gluconeogenesis. Gluconeogenesis occurs in the liver. With prolonged fasting the body is able to shift to using ketone bodies as fuel. Ketone bodies are derived from fatty acids, which we have stored in great abundance in our fat tissue. By shifting to ketone bodies, the need to make glucose and break down muscle protein declines substantially. That explains why the rate of muscle protein loss (and thus muscle loss) is highest in the early days of (complete) fasting.

During fasting the body will favor the use of fatty acids as fuel. In fact, most tissues will adapt to rely almost exclusively on fatty acids. The problem is that some cells and tissues cannot use the fatty acids for fuel so they need glucose. Since after a few days of fasting the limited glucose stores (glycogen) are exhausted, the only option is to make glucose from amino acids. The amino acids originate from body protein, primarily muscle.

Q: It is said that bile contains bile acids, which are critical for digestion and absorption of fats and fat-soluble vitamins in the small intestine. In the course, I learned that the chemical digestion of nutrients is catalyzed by digestive enzymes. Therefore, I think that bile acids also contain digestive enzymes? Thus liver also produces digestive enzymes?

A: Bile acids are necessary for digestion of dietary fat but they cannot be considered as enzymes. They disperse the fat into smaller fat droplets so the pancreatic lipase enzyme has easier access. They also stimulate the activity of pancreatic lipase.

Q: Why do many people think that eating complex carbohydrates (as those found in bread, pasta or rice) is better than eating sugar. Why is eating long chains of glucose better for you than small ones, as in sucrose (I know it's glucose+fructose, but the second gets converted to glucose eventually)? By the time they leave the duodenum they have all been broken down into single molecules of sugar, so why the difference?

 A: The main issue is that complex carbohydrates usually come in a healthier "package". Foods rich in complex carbs such as grains often also contain plenty of fiber and micronutrients. That is not the case anymore when the grains are processed and you are left with white rice or white flour. Processing leads to loss of most of the valuable nutrients except starch and protein.

Q: Are fully hydrogenated oils simply saturated fats and have the same health effects as other saturated fats? 

Are trans fats (partially hydrogenated oils) more unhealthy than saturated fats for the heart disease? Both raise LDL, but trans also lower HDL, while saturated fats raise HDL. This is mentioned on many sites as the reason why trans fats are even for the heart than saturated fats, because HDL is beneficial. But the recent research questions the beneficial effect of HDL as is mentioned in this course. Assuming that HDL is not beneficial, are trans fats no more unhealthy for the heart than saturated fats?

Are industrial trans fats different chemically than natural trans fats from ruminant animals? Are they more unhealthy?

A: Yes. Fully hydrogenated means saturated. But the fatty acid started out as unsaturated and via full hydrogenation has been converted into a saturated fatty acid.

Yes, trans fatty acids are considered worse than saturated fatty acids. There is broad consensus on this notion.

Partly. The main industrial trans fatty acid is elaidic acid. The main natural trans fatty acids (formed in the stomach of ruminants) are vaccenic acid and conjugated linoleic acid. It still hasn't been fully resolved whether natural trans fatty acids are as bad as industrial trans fatty acids but recent evidence seems to indicate that they are not. But it is too early to reach definitive conclusions.

We recently did an analysis for partially hydrogenated soybean oil (a notorious source of trans fatty acids) and found loads of different trans fatty acids besides elaidic acid. Some of the trans fatty acids were so obscure that we couldn't even properly identify them.

Q: In cohort study you say that we maximally reduce the effects of confounders in analysis of data but my point of confussion is that why we have to eliminate the confounder from data as they are giving us information related to the causes of certain disease?

A: Let me try to explain. Imagine that you are interested in the relation between alcohol consumption and heart disease. In that case, you need to correct for factors that influence heart disease and that may be linked to alcohol consumption. A good example is smoking. Smoking promotes heart disease and is associated with alcohol consumption. People that drink alcohol are more likely to smoke. If you wouldn't correct for smoking you will likely reach the conclusion that alcohol increases the risk for heart disease, while in fact that relation only exists because people that drink alcohol are more likely to smoke. We would like to know if alcohol by itself influences risk for heart disease. To properly answer that question it is necessary to consider smoking a confounder and correct for it in the analysis.

Q: After heating oil in a pan, and letting it cool again, it becomes more viscous than fresh oil. Since we are told that trans fats have a higher melting point, I thought that this high viscosity stems from part of the oil becoming trans. You say (in the quiz) that conversion from cis to trans cannot happen during regular cooking. Then what does happen to the oil during cooking? Anyone can see that something happened to it, and it is not the same.

A: The stability of an oil during frying depends on many factors including type of oil, the rate of replenishment with fresh oil, frying conditions, original quality of frying oil, food materials, type of fryer, antioxidants, and the oxygen concentration. One of the reactions that occurs is the breakdown (hydrolysis) of the triglyceride molecule into fatty acids and mono- and diglycerides. The fatty acids will be subject to oxidation and may be converted into a variety of degradation products, which have a negative effect on the taste and smell of the oil.

Still, you don't form trans fatty acids by simply heating up the oil in a frying pan, even at relatively high temperatures.

[Cut & pasted from FAQ:]

\19. If fat cannot be converted to glucose how can fat then be turned into energy?
You can compare it with a car which drives on fuel. A car can drive on different fuels. Which fuel the car uses is dependent on what we put in it. In the same manner, what we use to obtain energy is dependent on what we eat. However, when our energy intake is lower than our energy needs we have to rely on our energy storage. In this case, fat is the most readily available source of energy. This explains why we mainly use fat as an energy source.
In more detail: When our energy intake is lower than our needs we start to break down triglycerides from our adipose tissue. The breakdown of triglycerides gives glycerol and free fatty acids. The glycerol can be converted into a glycolysis intermediate (dihydroxyacetone phosphate). The dihydroxyacetone phosphate can be converted into pyruvic acid through the glycolysis pathway and further oxidized in the citric acid cycle and electron transport chain to provide energy (ATP). The free fatty acids are oxidized to acetyl CoA in the mitochondria using beta-oxidation. The acetyl-CoA is then converted into energy (ATP) using the citric acid cycle and the electron transport chain.

#### remedial reading

Basic function of the alimentary tract
The picture below shows the main features of the alimentary tract, which include the mouth, stomach, small intestine and large intestine. Food normally stays in the mouth for only a limited amount of time. Via the esophagus the chewed up food is taken to the stomach where it stays anywhere between several minutes and a few hours. Food is released into the small intestine in phases, first arriving in the duodenum, followed by the jejunum and the ileum. Complete digestion and absorption of a meal may take hours, depending on the size of the meal and its content. The residual non-digestible food components reach the colon, where they reside for 1-2 days and become a major part of the feces. The feces collect in the rectum and finally leave the body via the anus.



The alimentary tracts serves to break down foods into their basic components - a process referred to as digestion - and carry out the absorption of the basic components into the body. Digestions consists of mechanical breakdown of foods into smaller particles via the action of the teeth and via contractions of the muscles along the alimentary tract. In addition, digestion relies on chemical breakdown of the macronutrients protein, fat and carbohydrate from more complex molecular structures into their basic components: amino acids, fatty acids and glucose. When digestion is complete, the basic components are taken up into intestinal cells and subsequently distributed throughout the body.

Chemical breakdown is mediated by digestive enzymes produced by various cells and tissues along the alimentary tract. In biochemical terms, digestive enzymes function as catalysts by dramatically increasing the rate of the reaction via lowering of the activation energy, which represents an energy barrier that prevents a chemical reaction from proceeding. All reactions catalyzed by digestive enzymes represent hydrolysis. Hydrolysis describes the cleavage of a chemical bond by the addition of water.

Chemical digestion by endogenous digestive enzymes occurs in all major parts of the alimentary tract, with the exception of the colon. The only macronutrient that undergoes quantitatively significant digestion in the mouth is carbohydrate in the form of starch. The stomach importantly participates in protein digestion and has a minor role in fat digestion. Most of the chemical digestion takes place in the upper small intestine via the action of enzymes produced by either the pancreas or enterocytes lining the intestinal tract. The intestine is a major site of digestion of remaining undigested food components, mainly in the form of indigestible carbohydrates, via the activity of large numbers of bacteria residing in the colon.

Key structures of the alimentary tract
Mouth
Our teeth are responsible for breaking down solid foods into smaller pieces that can be swallowed, a process referred to as mastication. Teeth are firmly rooted into the underlying bone and are covered with a hard substance called enamel. Enamel is mainly composed of calcium phosphate and is the hardest substance in the human body. The food in our mouth is moisturized by the addition of saliva produced by the salivary glands. The saliva is mixed with the food via chewing motions and via movement of the tongue.



Saliva is 99% water, with the remainder consisting of salts, mucus (mucopolysaccharides and glycoproteins), enzymes (principally amylase), and antibacterial compounds such as lysozyme. Saliva is produced by salivary glands located underneath the tongue (sublingual gland), beneath the floor of the mouth (submandibular gland, primary producer), and around the ear (parotid gland). Production of saliva is triggered by the presence of food in the mouth but can also be stimulated by merely smelling food or thinking about food. Its primary role is to lubricate the food to allow easy passage through the esophagus. Additionally, saliva initiates the chemical digestion of food, has an anti-microbial function, and is important for tasting.

Esophagus
The esophagus is a hollow tube that transports the food bolus from the mouth to the stomach. The esophagus is surrounded at the top by the upper esophageal sphincter, a muscular ring that closes the esophagus when no food is being swallowed. The esophagus is bordered at the bottom by the lower esophageal sphincter, which prevents reflux of food from the stomach. Swallowing causes both sphincters to relax, allowing the passage of food from mouth to stomach. When food is swallowed, a leave-shaped flap called the epiglottis covers the larynx, thereby preventing food from entering the trachea. The walls of the esophagus consists of muscles that propel the food downwards via a motion called peristalsis.

Stomach
The stomach is a hollow bag with a very muscular wall that is situated between the esophagus and the duodenum. In adult humans, the volume of an empty stomach is probably only around 50 mL, which can expand to one liter or beyond after a copious meal. The main role of the stomach is in protein digestion. The wall of the stomach produces hydrochloric acid accounting for an unique feature of the interior of the stomach: its high acidity. Food usually remains in the stomach anywhere from several minutes to a few hours. The stomach is bordered by two gatekeepers that keep the food inside the stomach: the esophageal sphincter, and the pyloric sphincter.



The layer of the stomach is composed of four main types of the cells. The chief cells are responsible for the production of pepsinogen, one of the three most important enzymes in the alimentary tract involved in protein breakdown. Pepsinogen is a so called pro-enzyme (zymogen) that is self-activated into the functional enzyme pepsin in the highly acidic interior of the stomach. Parietal cells produce the hydrochloric acid that accounts for the high acidity of the stomach. Goblet cells are spread around the entire intestinal wall and produce mucus. In the stomach they are also referred to as foveolar cells. The mucus covers the interior lining of the stomach and protects it from the acid environment and from degradation by the proteolytic activity of pepsin. Finally, endocrine-paracrine cells are responsible for the production of a variety of hormones such as ghrelin and gastrin.



Small intestine: duodenum, jejunum, ileum
The small intestine can be separated into three continuous parts. The duodenum is a hollow tube about 30 cm long that connects the stomach to the jejunum. The digestive juices from the pancreas and the bile produced in the liver enter the alimentary tract at the level of the duodenum. The digestive juices entering the duodenum contain a lot of bicarbonate that serves to neutralize the acid food bolus entering from the stomach to enable continuation of food digestion. Besides being the site of food digestion, the duodenum controls the rate of emptying of the stomach by producing the hormones secretin and cholecystokinin, which is triggered by food entering the duodenum. Secretin and cholecystokinin have a variety of other actions including stimulating the pancreas to release bicarbonate and digestive enzymes.

The jejunum is the middle section of the small intestine in between the duodenum and ileum. It is a major site of nutrient digestion to generate the basic chemical components that the body can absorb. The ileum is the last portion of the small intestine where most of nutrient absorption takes place. 

Colon and rectum
After passing through the ileum, the intestinal content moves into the colon. The main function of the colon is to absorb water from the remaining indigestible food matter and serve as vehicle for the body to rid itself of fecal waste material. The colon is characterized by the presence of large numbers of bacteria referred to as the gut microbiota, which carry out the fermentation of dietary fiber. The bacterial composition of the gut microbiota is highly dependent on the composition of the diet.



The fecal matter collects into descending colon and rectum and is released from the body upon defacation.The movement of the fecal matter through the colon is accomplished by peristalsis. Peristalsis describes the sequential contraction and relaxation of the circular muscles surrounding the intestine. It propagates in a wave and causes the forward propulsion of the food bolus throug the GI tract.

As the wall of the rectum expands due to its filling, stretch receptors are activated leading to the desire to defecate. Contraction of the (external) anal sphincter muscle (a circular muscle surrounding the anal canal) prevents feces from coming out of the rectum. Defecation requires the relaxation of the anal sphincter and is assisted by contraction of the abdominal muscle to increase intra-abdominal pressure.

Food digestion and absorption
Digestion
Food digestion is primarily performed by digestive juices secreted by the pancreas and the small intestine. The pancreas is located behind the stomach in the bend of the duodenum. It produces several digestive enzymes (lipases, amylases, proteases). Pancreatic juice is also rich in bicarbonate. The pancreatic secretions collect in the pancreatic duct and drain into the duodenum through the Ampulla of Vater.

The enterocytes of the small intestine produce a variety of enzymes (disaccharidases, peptidases) that are responsible for the final steps of nutrients digestion into the basic chemical components (amino acids, glucose, fatty acids).



The liver produces bile. Excess bile is stored in the gall bladder, a hollow organ that lies just beneath the right lobe of the liver. Entry of food into the duodenum stimulates the secretion of cholecystokinin by the duodenum, which triggers the contraction of the gall bladder and causes release of bile into the duodenum. Bile participates in digestion of dietary fat by functioning as a emulsifier. The active components in bile are bile acids, phospholipids and cholesterol.

Food digestion and absorption will be covered in more detail in the sections on carbohydrate, lipids and protein.

Histology of the alimentary tract
In order to increase the absorptive capacity of the intestine, its surface area of the interior lining of the small intestine is greatly amplified by three main anatomical features: folds, villi, and microvilli. Intestinal folds are visible to the eye and can be considered as wrinkles of the entire intestinal wall. Villi are finger-like projections of the intestinal wall. Each villus (singular of villi) has a network of incoming and outgoing blood vessels and a lymphatic vessel called lacteal. Nutrients such as glucose and amino acids are taken up by the enterocyte and transported into the blood vessel (capillary). The absorbed nutrients are transported via the blood to different parts of the body as fuel or building block. Microvilli  are microscopically small protrusions of the cell membrane of enterocytes that face the lumen of the small intestine. Microvilli form the so called brush border. Numerous digestive enzymes produced by enterocytes are “captured” in the brush border.



Absorbed fats go into small lymphatic vessels within the villi called lacteals. The lacteals collect into larger mesenteric lymph ducts and finally into the large thoracic duct, which runs parallel to the aorta. The content of the thoracic duct drains into the circulation at the subclavian vein.

Intestinal villi are mainly covered by enterocytes. Besides producing a variety of digestive enzymes, enterocytes are the main absorptive cells in the intestine. Other cell types found on or just below the surface of the villi are mucus producing Goblet cells cells, hormone producing entero-endocrine cells, and Paneth cells, which secrete anti-bacterial lysozyme. The valley-like structures between the villi are called crypts and harbor the stem cells. These stem cells continuously give rise to new enterocytes, Goblet cells and entero-endocrine cells, which subsequently rise from the bottom of the crypts to the top of the villi, where they are sloughed off and released into the lumen. The self-renewal capacity of the intestinal stem cells is dependent on interaction with adjacent Paneth cells.



Entero-endocrine cells are specialized cells located throughout the alimentary tract, representing approximately 1% of the total epithelial cell population. In response to nutritional stimuli entero-endocrine cells release peptides referred to as gut hormones. It is estimated that at least 15 different subtypes of entero-endocrine cells exist, each of which is capable of secreting multiple hormones.  Gut hormones influence a variety of different functions of the alimentary tract including motility, satiety, and the secretory function of other cell types. Well known gut hormones include secretin, gastrin, cholecystokinin, glucagon-like peptide, and ghrelin.

Absorbed nutrients that are water soluble (e.g. amino acids, glucose, electrolytes) enter into capillaries that combine to become veins (shown in blue). The veins merge into the portal vein, which delivers the absorbed nutrients to the liver. Since portal blood is low in oxygen, oxygenated blood is delivered to the liver via the portal artery. The portal vein also carries the venous blood coming from the mesenteric and omental fat (=visceral fat).



Malfunctioning of the alimentary tract
Many people suffer from health problems related to the alimentary tract. The most commonly experienced problem is abdominal pain. Abdominal pain can be caused by a multitude of underlying defects and may reflect a minor transient disorder or a serious disease. Common causes of abdominal pain include constipation, indigestion, bacterial and viral infections, lactose intolerance, and menstrual cramps.

Diarrhea describes the condition of having loose watery stools that often requires frequent bathroom visits. Chronic diarrhea can lead to dehydration and may be treated with oral rehydration solution.

Constipation describes the condition of having infrequent bowel movements (“number 2”) or bowel movements that are hard to pass. It can be acute or chronic. Constipation is the result of stools staying in the colon for too long, leading them to become dry and hard. Common causes of constipation include low fiber consumption, lack of physical activity, stress, medications, and disregarding the urge to defecate. Constipation is usually relieved by dietary changes (increased fiber consumption) and if persistent, can be treated with laxatives.

Hemorrhoids are swollen and inflamed veins around the anus or in the lower rectum. Hemorrhoids can cause discomfort, pain, and anal itching. A distinction can be made between internal and external hemorrhoids. Hemorrhoids are usually recognized by red blood on stools (not in stools). Common causes of hemorrhoids are chronic constipation, straining during bowel movements, and spending too much time on the toilet. There is no scientific evidence that spicy food can promote the development of hemorrhoids.  However, spicy food may increase the burning and itching sensation in people that already suffer from hemorrhoids.

Colorectal cancer is a common form of cancer that affects the colon or the rectum. Blood in the stools may signal colon cancer, although colon cancer is often undiagnosed, justifying the installation of large scale screening programs in many countries.  Screening occurs via high-sensitivity fecal occult blood testing, sigmoidoscopy, or colonoscopy, looking for polyps. People in Europe have a higher chance of developing colon cancer compared to people in Africa and Asia, most likely due to differences in dietary habits. 

###### Backlinks

[^1]: amount in: `(protein in g)/(body mass in kg)/(day (24h))`


<!--
### commented out
#### 7.3 Regulation of food intake

- Hunger is defined as the uncomfortable sensation caused by lack of food that makes people look for food. 
- Appetite is the integrated response to the sight, smell, thought, or taste of food that triggers eating.
    + Sometimes appetite is driven by hunger, but often it is due to cravings, habits, the availability of food, boredom, or other social and emotional factors. Hunger is a very dominant sensation that impairs a person’s ability to focus on other tasks

##### Satiation and satiety

- **Satiation** is often defined as the feeling of satisfaction and fullness that occurs during a meal and that makes you stop eating. **Satiation** determines how much food is consumed during a meal. It is your immediate reaction to the ingestion of food: the drive that causes you to stop eating. 
- **Satiety** is defined as the feeling of satisfaction that occurs after a meal and prevents you from eating until the next meal. **Satiety** determines how much time passes between meals. 
    + Please note that the distinction between satiation and satiety is not necessarily maintained in normal (colloquial) language and the two terms are often used interchangeably.

##### Factors determining food intake

- Sensory factors
    + taste, smell, sight, sound
- Social factors
    + culturally defined
    + we tend to eat more in company than when we eat alone
    + eating food together is used for social bonding and celebration of special occasions
    + **social factors cleary and easily override psysiology, at least in the short term.**
- Psychological factors
    + appetite is subject to stress. Some people lose their appetite when under stress, some people increase their food intake. "_The latter is often called stress eating or emotional eating and, if persistent, can cause major weight gain in some individuals_" (letterlijke quote, wtf => dat psychiatry: binge eating disorder, failure to cope, psychotraumata etc.)
- Physiological factors
    + Psysiology dictates food choice.
        * evolutionarily (survival, propagation, reproduction etc.)
            - The reason for this is believed to be that almost all people match their energy intake to their energy expenditure by evolutionary design. (`hunger => increase food intake ; full => decrease food intake`)

![Feedbackloop_food_intake](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/cbeac5f1e39bdd153e3b07a60c9f0817/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/6.1_slide_10.png)

> The picture shows the mechanisms that contribute to short term and long term regulation of food intake. Food intake will cause immediate distension (stretching) of the stomach that conveys the feeling of fullness. Entry of food into the GI tract will inhibit release of hunger hormones and stimulate release of satiety hormones. Well known satiety hormones include cholecystokinin (CCK) and glucagon-like peptide 1 (GLP-1). Entry of glucose and amino acids into the bloodstream will activate nutrient-sensing nerve cells in the hypothalamus.

##### Leptin #####

- \[leptin] is produced by fat tissue
- Production and blood levels of \[leptin] are proportional to the amount of fat mass.
    + These signals converge in satiety cells in the hypothalamus, provoking the feeling of **satiety**
    + Genetic defects can cause extreme hunger and leads to massive overfeeding and obesity

##### Hunger

- Low levels of nutrients in the blood + decrease of \[leptin] production triggers hunger cells in the hypothalamus, inducing strong hunger sensations.
    + This is a critical mechanism for survival. (i.e. this is the impulse which leads to bodyfat storage, which is (evolutionarily) essential for survival)

![feedback_loop_fat](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/a7983d639bf324c34985c1cd8570edae/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/6.1_slide_11.png)

> The adipose or fat tissue plays a central role in the long term regulation of food intake. When people gain bodyfat, leptin release from fat tissue is increased. The increased levels of leptin in the blood reach the hypothalamus where they stimulate satiety cells. Increased satiety leads to a reduction in food intake, which in turn causes decreased energy storage, closing the circle. When people lose bodyfat or fast, leptin release from fat tissues is decreased. Decreased levels of leptin in the blood stimulate hunger cells in the hypothalamus, provoking hunger. The hunger causes people to consume food, which in turn leads to increased energy storage, again closing the circle.
> 
> The reduction in leptin levels during weight loss is believed to be one of the reasons why people have difficulty maintaining weight loss.
-->

<!--
#### 7.4 Energy value of nutrients
-->

<!--
#### 7.5 Energy expenditure

##### Flow chart of energy

![Flow_chart_of_energy](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/13792938f259af4f1ab42d64901de188/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/MOOC13.jpg)

> The picture provides a graphical illustration of the different types of energy. In people that are in energy balance, the total amount of energy coming in (gross energy) is equal to the energy going out. The latter consists of two components: 1) energy lost in stools and urine, 2) energy lost as energy burned (expenditure). As indicated already, the energy content of foods and thus the calculation of our energy intake is based on metabolizable energy, which means that loss of energy in stools and urine has already been taken into account in the calculation of energy intake. Hence, in people that are in energy balance, energy intake (as metabolizable energy) equals energy expenditure (energy going out). The metabolizable energy is available for combustion to yield net energy.
> 
> Net energy is available for performing various tasks in the body. These energy-consuming tasks are divided into three main categories: 1) maintenance, 2) physical activity, 3) growth. Daily energy expenditure is about 2000 Kcal for an average female and about 2500 Kcal for an average male. These numbers can be substantially higher or lower depending on body size, illness and physical activity.

##### Maintenance

> BMR covers about 60-70% of our daily energy expenditure. The magnitude of the BMR is different between individuals and depends on the lean body mass, which is the mass of the body minus the fat. In simple terms, tall people with lots of muscle have a high BMR, whereas short people that carry little muscle have a low BMR.
> 
> The BMR can be estimated using different formulas. The best known formula is from **Harris-Benedict** , which is specified below:

In men: `66.5 + (13.75 X weight in kg) + (5.003 X height in cm) – (6.775 X age in years)`

In women: `655.1 + (9.563 X weight in kg) + (1.85 X height in cm) – (4.676 X age in years)`

An alternative is the Mifflin-St.Jeor formula:

In men: `10 x weight (kg) + 6.25 x height (cm) – 5 x age (y) + 5`

In women: `10 x weight (kg) + 6.25 x height (cm) – 5 x age (y) – 161`


##### Physical activity

![PE](https://prod-edge-static.edx-cdn.org/assets/courseware/v1/c18d7ba8f394814be68b44ca35bb8fc2/asset-v1:WageningenX+NUTR101+3T2019+type@asset+block/MOOC14.jpg)

`PAL = physical activity level = ( total energy expenditure ) / BMR `

- physhically very active: PAL>2.0
- extremely inactive: PAL<1.4
- avg: PAL=1.6--1.7

**Indirect calorimetry**

- Measuring O2-consumption and or CO2-excretion and counting calories from there.
    + E.g. Oxygen consumption is determined at 2L/min => 9.6 kcal/min ( 2 * 4.8 )
-->
